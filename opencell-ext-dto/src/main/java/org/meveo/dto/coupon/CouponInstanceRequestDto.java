package org.meveo.dto.coupon;

import org.meveo.api.dto.BaseEntityDto;

import lombok.Getter;
import lombok.Setter;

/**
 * A Dto encapsulating the Coupon Instance Request params
 */
@Getter
@Setter
public class CouponInstanceRequestDto extends BaseEntityDto {

    /**
     * The CouponTemplate's code which will be used.
     */
    private String code;

    /**
     * If true , hence the generated CouponInstance will returned
     */
    @Deprecated
    private Boolean returnInstances;
}
