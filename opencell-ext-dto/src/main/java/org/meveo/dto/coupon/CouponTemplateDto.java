package org.meveo.dto.coupon;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.meveo.api.dto.BaseEntityDto;
import org.meveo.api.dto.CustomEntityInstanceDto;
import org.meveo.api.dto.CustomFieldDto;
import org.meveo.api.dto.CustomFieldValueDto;
import org.meveo.api.dto.CustomFieldsDto;
import org.meveo.api.dto.EntityReferenceDto;
import org.meveo.model.catalog.OfferTemplate;

import org.meveo.common.CouponDiscountTypeEnum;
import org.meveo.common.CouponStatusEnum;
import org.meveo.common.CouponTypeEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponTemplateDto extends BaseEntityDto {

    public static final String CET_CODE = "CouponTemplate";
    public static final String CF_STATUS = "status";
    public static final String CF_VALID_FROM = "validFrom";
    public static final String CF_VALID_TO = "validTo";
    public static final String CF_COUPON_TYPE = "couponType";
    public static final String CF_PREFIX = "prefix";
    public static final String CF_QTE = "quantity";
    public static final String CF_DISCOUNT_TYPE = "discountType";
    public static final String CF_DISCOUNT_VALUE = "discountValue";
    public static final String DESCRIPTION = "description";

    private String code;
    private String description;
    private Boolean disabled;
    private String prefix;
    private Long quantity;
    private Date validFrom;
    private Date validTo;
    private boolean activateNow;
    private Double discountValue;
    private Long duration;
    private Map<String, String> descriptionI18n;
    private boolean paidCampaign;
    private CouponTypeEnum couponType;
    private CouponDiscountTypeEnum discountType;
    private CouponStatusEnum status;

    private List<String> offers;

    public CustomEntityInstanceDto toCustomEntityInstanceDto() {
        CustomEntityInstanceDto ceiDto = new CustomEntityInstanceDto();

        ceiDto.setCetCode(CET_CODE);
        ceiDto.setCode(this.code);
        ceiDto.setDescription(this.description);
        ceiDto.setDisabled(this.disabled != null ? this.disabled : false);

        List<CustomFieldDto> customField = new ArrayList<>();

        // Adding offers CF
        if (CollectionUtils.isNotEmpty(offers)) {
            List<CustomFieldValueDto> offerCFValues = new ArrayList<>();
            offers.stream().forEach(o -> {
                EntityReferenceDto entityReferenceDto =  new EntityReferenceDto();
                entityReferenceDto.setClassname(OfferTemplate.class.getName());
                entityReferenceDto.setCode(o);
                offerCFValues.add(new CustomFieldValueDto(entityReferenceDto));
            });

            CustomFieldDto offersCF = new CustomFieldDto();
            offersCF.setCode("offers");
            offersCF.setListValue(offerCFValues);
            customField.add(offersCF);
        }

        // Adding descriptionI18n CF
        if (MapUtils.isNotEmpty(this.descriptionI18n)) {
            CustomFieldDto descriptionI18nCF = new CustomFieldDto();
            LinkedHashMap<String, CustomFieldValueDto> mapValue = new LinkedHashMap<>();

            this.descriptionI18n.entrySet().stream().forEach(entry -> {
                CustomFieldValueDto value = new CustomFieldValueDto();
                value.setValue(entry.getValue());
                mapValue.put(entry.getKey(), value);
            });

            descriptionI18nCF.setCode("descriptionI18n");
            descriptionI18nCF.setMapValue(mapValue);
            customField.add(descriptionI18nCF);
        }

        // Adding duration CF
        CustomFieldDto durationCF = new CustomFieldDto();
        durationCF.setCode("duration");
        durationCF.setLongValue(this.duration);
        customField.add(durationCF);

        // Adding discountValue CF
        CustomFieldDto discountValueCF = new CustomFieldDto();
        discountValueCF.setCode(CF_DISCOUNT_VALUE);
        discountValueCF.setDoubleValue(this.discountValue);
        customField.add(discountValueCF);

        // Adding discountType CF
        CustomFieldDto discountTypeCF = new CustomFieldDto();
        discountTypeCF.setCode(CF_DISCOUNT_TYPE);
        discountTypeCF.setStringValue(this.discountType.getValue());
        customField.add(discountTypeCF);

        // Adding paidCampaign CF
        CustomFieldDto paidCampaignCF = new CustomFieldDto();
        paidCampaignCF.setCode("paidCampaign");
        paidCampaignCF.setStringValue(this.paidCampaign ? "YES" : "NO");
        customField.add(paidCampaignCF);

        // Adding activateNow CF
        CustomFieldDto activateNowCF = new CustomFieldDto();
        activateNowCF.setCode("activateNow");
        activateNowCF.setStringValue(this.activateNow ? "YES" : "NO");
        customField.add(activateNowCF);

        // Adding validTo CF
        CustomFieldDto validToCF = new CustomFieldDto();
        validToCF.setCode(CF_VALID_TO);
        validToCF.setDateValue(this.validTo);
        customField.add(validToCF);

        // Adding validFrom CF
        CustomFieldDto validFromCF = new CustomFieldDto();
        validFromCF.setCode(CF_VALID_FROM);
        validFromCF.setDateValue(this.validFrom);
        customField.add(validFromCF);

        // Adding quantity CF
        CustomFieldDto quantityCF = new CustomFieldDto();
        quantityCF.setCode("quantity");
        quantityCF.setLongValue(this.quantity);
        customField.add(quantityCF);

        // Adding status CF
        CustomFieldDto statusCF = new CustomFieldDto();
        statusCF.setCode(CF_STATUS);
        statusCF.setStringValue(this.status.getValue());
        customField.add(statusCF);

        // Adding couponType CF
        CustomFieldDto couponTypeCF = new CustomFieldDto();
        couponTypeCF.setCode(CF_COUPON_TYPE);
        couponTypeCF.setStringValue(this.couponType.getValue());
        customField.add(couponTypeCF);

        // Adding prefix CF
        CustomFieldDto prefixCF = new CustomFieldDto();
        prefixCF.setCode(CF_PREFIX);
        prefixCF.setStringValue(this.prefix);
        customField.add(prefixCF);

        CustomFieldsDto customFields = new CustomFieldsDto();
        customFields.setCustomField(customField);
        ceiDto.setCustomFields(customFields);

        return ceiDto;
    }

    @Override
    public String toString() {
        return "CouponTemplateDto{" +
                "code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", disabled=" + disabled +
                ", prefix='" + prefix + '\'' +
                ", quantity=" + quantity +
                ", validFrom=" + validFrom +
                ", validTo=" + validTo +
                ", activateNow=" + activateNow +
                ", discountValue=" + discountValue +
                ", duration=" + duration +
                ", descriptionI18n=" + descriptionI18n +
                ", paidCampaign=" + paidCampaign +
                ", couponType=" + couponType +
                ", discountType=" + discountType +
                ", status=" + status +
                ", offers=" + offers +
                '}';
    }
}
