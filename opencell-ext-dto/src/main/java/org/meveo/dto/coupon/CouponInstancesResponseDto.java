package org.meveo.dto.coupon;

import java.util.ArrayList;
import java.util.List;

import org.meveo.api.dto.response.SearchResponse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponInstancesResponseDto extends SearchResponse {

    private List<CouponInstanceDto> couponInstances;
    public CouponInstancesResponseDto() {
        this.couponInstances = new ArrayList<>();
    }

    public void addItem(CouponInstanceDto dto) {
        this.couponInstances.add(dto);
    }
}
