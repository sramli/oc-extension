package org.meveo.dto.coupon;

import java.util.ArrayList;
import java.util.List;

import org.meveo.api.dto.BaseEntityDto;
import org.meveo.api.dto.CustomEntityInstanceDto;
import org.meveo.api.dto.CustomFieldDto;
import org.meveo.api.dto.CustomFieldsDto;
import org.meveo.api.dto.EntityReferenceDto;
import org.meveo.model.crm.custom.CustomFieldValues;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponInstanceDto extends BaseEntityDto {

    private static final Logger LOGGER = LoggerFactory.getLogger(CouponInstanceDto.class);
    public static final String CET_CODE = "CouponInstance";
    public static final String CF_COUPON_TEMPLATE = "couponTemplate";
    private static final String CF_INITIAL_QUANTITY = "initialQuantity";
    public static final String CF_REMAINING_QUANTITY = "remainingQuantity";

    private String code;
    private String couponTemplate;
    private String description;
    private Long initialQuantity;
    private Long remainingQuantity;

    public CouponInstanceDto() {
    }

    public CouponInstanceDto(CustomEntityInstance couponInstance) {
        try {
            CustomFieldValues fieldValues = couponInstance.getCfValuesNullSafe();
            this.code = couponInstance.getCode();
            this.description = couponInstance.getDescription();
            this.initialQuantity = (Long) fieldValues.getValue(CF_INITIAL_QUANTITY);
            this.remainingQuantity = (Long) fieldValues.getValue(CF_REMAINING_QUANTITY);
            this.couponTemplate = (String) couponInstance.getCfValuesNullSafe().getValue(CF_COUPON_TEMPLATE);
        } catch (Exception e) {
            LOGGER.error("Error constructing CouponInstanceDto from couponInstance = {}", couponInstance, e);
        }
    }

    public CustomEntityInstanceDto toCustomEntityInstanceDto() {
        CustomEntityInstanceDto ceiDto = new CustomEntityInstanceDto();

        ceiDto.setCetCode(CET_CODE);
        ceiDto.setCode(this.code);
        ceiDto.setDescription(this.description);

        List<CustomFieldDto> customField = new ArrayList<>();

        // adding couponTemplate CF
        EntityReferenceDto couponTemplateRef =  new EntityReferenceDto();
        couponTemplateRef.setClassname(CustomEntityInstance.class.getName());
        couponTemplateRef.setCode(this.couponTemplate);

        CustomFieldDto couponTemplateCF = new CustomFieldDto();
        couponTemplateCF.setCode(CF_COUPON_TEMPLATE);
        couponTemplateCF.setEntityReferenceValue(couponTemplateRef);
        customField.add(couponTemplateCF);

        // Adding initialQuantity CF
        CustomFieldDto initialQuantityCF = new CustomFieldDto();
        initialQuantityCF.setCode(CF_INITIAL_QUANTITY);
        initialQuantityCF.setLongValue(this.initialQuantity);
        customField.add(initialQuantityCF);

        // Adding remainingQuantity CF
        CustomFieldDto remainingQuantityCF = new CustomFieldDto();
        remainingQuantityCF.setCode(CF_REMAINING_QUANTITY);
        remainingQuantityCF.setLongValue(this.remainingQuantity);
        customField.add(remainingQuantityCF);

        CustomFieldsDto customFields = new CustomFieldsDto();
        customFields.setCustomField(customField);
        ceiDto.setCustomFields(customFields);

        return ceiDto;
    }
}
