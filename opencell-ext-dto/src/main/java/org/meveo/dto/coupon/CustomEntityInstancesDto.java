package org.meveo.dto.coupon;

import java.util.ArrayList;
import java.util.List;

import org.meveo.api.dto.CustomEntityInstanceDto;
import org.meveo.api.dto.response.SearchResponse;

public class CustomEntityInstancesDto extends SearchResponse {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -954637537391623234L;

    private List<CustomEntityInstanceDto> customEntityInstanceDtos= new ArrayList<>();

    public List<CustomEntityInstanceDto> getCustomEntityInstanceDtos() {
        return customEntityInstanceDtos;
    }

    public void setCustomEntityInstanceDtos(List<CustomEntityInstanceDto> customEntityInstanceDtos) {
        this.customEntityInstanceDtos = customEntityInstanceDtos;
    }
}
