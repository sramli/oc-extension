package org.meveo.dto.walletoperation;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.meveo.common.DateUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Transfert object pour affichage des charges dans le justificatif de paiement
 * 
 * @author c.lafarge
 *
 */
@Setter
@ToString
public class JustificatifPaiementDto {

	/**
	 * code sous catégorie
	 */
	private String code;

	/**
	 * Date opération
	 */
	private Date operationDate;

	/**
	 * Date de début
	 */
	private Date startDate;

	/**
	 * Date de fin d'abonnement (peut être vide)
	 */
	private Date endDate;

	@Getter
	private Date invoiceDate;

	/**
	 * Nom de l'abonnement, doit être traduit
	 */
	private String description;

	private String chargeLabel;
	
	private String parameter3;

	/**
	 * Le montant réglé
	 */
	private BigDecimal montant;
	
	/**
	 * Id de la souscription
	 */
	private String subscriptionId;
	
	/**
	 * Code coupon
	 */
	private String codeCoupon;

	public JustificatifPaiementDto() {
	}

	public JustificatifPaiementDto(String code, Date startDate, Date endDate, String description, BigDecimal montant) {
		this.code = code;
		this.startDate = startDate;
		this.endDate = endDate;
		this.description = description;
		this.montant = montant;
	}

	public String getCode() {
		return StringUtils.defaultString(code);
	}

	public String getOperationDate() {
		return formatDate(this.operationDate);
	}

	public String getStartDate() {
		return formatDate(this.startDate);
	}
	
	public long getStartDateAndTime() {
		return this.startDate.getTime();
	}
	
	public Date getStartDateAsDate() {
		return this.startDate;
	}

	public String getEndDate() {
		return formatDate(this.endDate);
	}
	
	public long getEndDateAndTime() {
		return this.endDate.getTime();
	}
	
	public Date getEndDateAsDate() {
		return this.endDate;
	}

	public String formatInvoiceDate() {
		return formatDate(this.invoiceDate);
	}

	public String getDescription() {
		return StringUtils.defaultString(description);
	}

	public String getChargeLabel() {
		return StringUtils.defaultString(chargeLabel);
	}

	public String getMontant() {
		return formatMontant(this.montant);
	}
	
	public String getSubscriptionId() {
		return StringUtils.defaultString(subscriptionId);
	}

	public String getCodeCoupon() {
		return StringUtils.defaultString(codeCoupon);
	}

	public BigDecimal getMontantAsBigDec() {
		return this.montant;
	}
	
	public String getParameter3() {
		return this.parameter3;
	}

	public static String formatDate(Date date) {
		return null == date ? "" : DateUtils.formatDateWithPattern(date, DateUtils.PDF_DATE_PATTERN);
	}

	public static String formatMontant(BigDecimal amount) {
		return null == amount ? "" : new DecimalFormat("#,##0.00").format(amount) + " €";
	}
}
