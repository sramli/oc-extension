package org.meveo.common;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonValue;
import com.google.common.collect.ImmutableMap;

public enum CouponTypeEnum {

    GENERIC("generic") , UNIQUE("unique");

    CouponTypeEnum (String value) {
       this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    private String value;
    public static final Map<String,CouponTypeEnum> instances = ImmutableMap.of("generic", GENERIC, "unique", UNIQUE);
}
