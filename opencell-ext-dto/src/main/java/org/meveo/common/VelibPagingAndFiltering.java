package org.meveo.common;

import java.util.Map;

import org.meveo.api.dto.response.PagingAndFiltering;

import com.google.common.collect.ImmutableMap;

public class VelibPagingAndFiltering extends PagingAndFiltering {

    public VelibPagingAndFiltering () {
        super();
    }
    /**
     * Map holding the different operations to use for an NUMERIC filter (or may be a DATE also)
     */
    public static final Map<String,String> CF_CRETERIA_OPERATION_MAP = ImmutableMap.of("gs", ">", "ge", ">=", "ls", "<", "le" , "<=" , "eq" , "=");

    /**
     * Map to send filters based on the Custom Fields of the Entity
     */
    private Map<String, Object> cfFilters;

    public Map<String, Object> getCfFilters() {
        return cfFilters;
    }

    public void setCfFilters(Map<String, Object> cfFilters) {
        this.cfFilters = cfFilters;
    }
}
