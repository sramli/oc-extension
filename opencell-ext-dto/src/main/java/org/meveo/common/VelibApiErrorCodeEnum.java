package org.meveo.common;
import javax.ws.rs.core.Response;

import org.meveo.api.ApiErrorCodeEnum;

public enum VelibApiErrorCodeEnum implements ApiErrorCodeEnum {

    GENERIC_API_EXCEPTION("GENERIC API EXCEPTION"),
    ERR01("Missing mandatory field", Response.Status.BAD_REQUEST),
    ERR02("Invalid Usage access date"),
    ERR03("Bike.electrical value error"),
    ERR04("DepartureFeature.battery.electrical value error"),
    ERR05("DepartureFeature.dock.electrical value error"),
    ERR06("DepartureFeature.date format error"),
    ERR07("ArrivalFeature.battery.electrical value error"),
    ERR08("ArrivalFeature.dock.electrical value error"),
    ERR09("ArrivalFeature.date format error"),
    ERR10("Created date format error"),
    ERR11("TechnicalRessource.electrical value error"),
    ERR12("Unknown userId"),
    ERR13("DepartureUsage Id  already exist"),
    ERR14("ArrivalUsage Id  already exist"),
    ERR15("Unknown customerExternalCode"),
    ERR16("Unknown customerInternalCode", Response.Status.NOT_FOUND),
    ERR22("Name.title value error"),
    ERR23("PreferredLanguage value error"),
    ERR24("BirthDate format error"),
    ERR26("login already exist error"),
    ERR27("Email format error"),
    ERR28("Password format error"),
    ERR29("PinCode format error"),
    ERR30("Origin value error"),
    ERR31("Id null or value error"),
    ERR32("Identification.type value error"),
    ERR33("ActivationDate format error"),
    ERR34("No active subscription error"),
    ERR35("Missing Access Point "),
    ERR40("creditCategory value error"),
    ERR41("DunningLevel value error"),
    ERR42("BikeForbidden value error"),
    ERR43("EmailValidationTag value error"),
    ERR44("PhoneValidationTag value error"),
    ERR45("CertificatStatusValidationTag value error"),
    ERR46("IdentityDocValidationTag value error"),
    ERR47("BankDocValidationTag  value error"),
    ERR48("EbikeLevel value error"),
    ERR49("Updated.date format error"),
    ERR50("Technical Error", Response.Status.INTERNAL_SERVER_ERROR),
    ERR51("Missing parameters"),
    ERR52("General Terms statut value error"),
    ERR53("Usage NFC value error"),
    ERR54("Preferred Card Support value error"),
    ERR55("Allow Velib SMS value error"),
    ERR56("Allow Velib Mail value error"),
    ERR57("Allow Partenaire SMS value error"),
    ERR58("Allow Partenaire Mail value error"),
    ERR59("Suspension Type value error"),
    ERR60("Bonus Type value error"),
    ERR61("Unknown offerCode"),
    ERR62("Unknown discountCode"),
    ERR63("Missing Offer configuration error"),
    ERR64("Subscription error"),
    ERR65("Authorization boss error"),
    ERR66("Suspension value error : value must be \'yes\' or \'no\'"),
    ERR67("Bonus value error : value must be a number"),
    ERR80("Order validation error : invalide offer date"),
    ERR81("Order validation error : invalide offer status"),
    ERR82("Order validation error : one active subscription constraint"),
    ERR83("Order validation error : missing pin code"),
    ERR84("Unknown customerCardDetails.cardTypeBrand"),
    ERR85("Unknown customerCardDetails.cardownerId"),
    ERR86("Missing customerCardDetails mandatory field"),
    ERR87("customerCardDetailscardYearExpirationEd format error"),
    ERR88("customerCardDetails.cardMonthExpirationEd format error"),
    ERR89("Unknown customerQuotationId"),
    ERR90("Unknown customerInternalCode"),
    ERR91("Policy Constraint : not allowed to use old password"),
    ERR92("Data inconsistency : login with customerInternalCode"),
    ERR93("resetWithoutPassword value error"),
    ERR94("issueType value error"), ERR95("Unknown subscriptionCode"), ERR96("Unknown paymentReference"),
    ERR97("productCode value error"),
    ERR98("Unknown optionalSProductToActivate.productCode"),
    ERR99("AutomaticRenewSubscriptionTag value error"),
    ERR114("DepartureFeature.station.status value error"),
    ERR115("ArrivalFeature.station.status value error"),
    ERR100("cardResponseUpdatedDate format error", Response.Status.BAD_REQUEST),
    ERR101("cardResponseStatus value error", Response.Status.BAD_REQUEST),
    ERR102("Unknown cardRequestNumber", Response.Status.NOT_FOUND),
    ERR103("Data inconsistency : cardRequestNumber with customerInternalCode", Response.Status.BAD_REQUEST),
    ERR117("Unknown device operating system value"),
    ERR118("Unknown device token value"),
    ERR119("Unknown customerExternalAccessId"),
    ERR120("Login empty not allowed"),
    ERR125("Unknow offerTemplateCode"),
    ERR126("Offer not actually active"),
    ERR127("Incoherent data : only one payment allowed CARD or BANK"),
    ERR128("Missing customerBankDetails mandatory field"),
    ERR129("customerBankDetails.iban format error"),
    ERR130("customerBankDetails.mandatDate format error"),
    ERR131("customerBankDetails.key format error"),
    ERR132("customerBankDetails.mandateIdentification format error"),
    ERR133("customerBankDetails.Bic format error"),
    ERR134("Station already exists in Favorites"),
    ERR135("Station limit number reached"),
    ERR136("Unknown stationId"),
    ERR137("OfferUpgradeDetails.offerUgradeTimeExecution format error"),
    ERR138("Offer not active at the chosen date"),
    ERR139("Offer upgrade date is after the end of the subscription"),
    ERR140("Offer upgrade already exists for the customer"),
    ERR141("Offer code is V-MAX"),
    ERR145("Unknown notification value", Response.Status.NOT_FOUND),
    ERR146("Unknown notification type", Response.Status.NOT_FOUND),
    ERR147("Failure when sending mail"),
    ERR148("Mismatch type/value for notification"),
    ERR149("FromDepartureDate format error"),
    ERR150("ToDepartureDate format error"),
    ERR151("Incoherence between FromDeparture  and ToDeparture Date"),
    ERR152("multiple active access point found for the userAccount with code"),
    ERR153("ArrivalUsage Id doesn't exist"),
    ERR154("Amount of ordered cards is not correct"),
    ERR155("Existing pairing for this card"),
    ERR156("The promo code entered is invalid or expired"),
    ERR157("No offer templates found"),
    ERR158("Multiple offer templates found"),
    ERR159("The promo code entered is no longer available"),
    ERR160("PaymentMethod doesn't belong to any CustomerAccount"),
    ERR161("IBAN payment method doesn't exist"),
    ERR162("Sending email is not allowed for customer "),
    ERR163("Error when sending email with iban information for customer "),
    ERR164("IOException when sending email for payment suspension for customer ");

    private String description;

    private Response.Status responseStatus;

    VelibApiErrorCodeEnum(String description) {
        this.description = description;
    }

    VelibApiErrorCodeEnum(String description, Response.Status responseStatus) {
        this.description = description;
        this.responseStatus = responseStatus;
    }

    public String getDescription() {
        return description;
    }

    public Response.Status getResponseStatus() {
        return responseStatus;
    }
}
