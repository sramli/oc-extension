package org.meveo.common;

import java.util.Arrays;
import java.util.List;

public class ConstantesVelib {

    // AccountOperation transactionType
    public static final String RF_CARD = "RF";

    // Path to folder for report Job
    public static final String REPORT_JOB_ABSOLUTE_PATH = "/tmp/meveo/";
    public static final String REPORT_JOB_RETAKE_BONUS = "RetakeBonusJob";
    public static final String REPORT_JOB_REFUND = "RefundJob";
    public static final String REPORT_JOB_REFUND_VELIB = "RefundVelibJob";
    public static final String PATH_TO_REPORT_JOB = "CR_job/";

    // Offers' categories
    public static final String SHORT_DURATION_OFFER_CATEGORY = "SHORT_DURATION_OFFER";
    public static final String LONG_DURATION_OFFER_CATEGORY = "LONG_DURATION_OFFER";

    // Offers' services
    public static final String SERVICE_ABONNEMENT_TEMPLATE_CODE = "V1_SERVICE_ABONNEMENT";

    // Offers' custom fields
    public static final String CF_ANNUAL_SUBSCRIPTION_ACTIVATION = "V1_ACTIVATION_ABONNEMENT_ANNUEL";
    public static final String CF_MONTHLY_SUBSCRIPTION_ACTIVATION = "V1_ACTIVATION_ABONNEMENT_MENSUEL";

    // Billing cycle codes
    public static final String VAD_BILLING_CYCLE_CODE = "BILLCYCLE_VAD";
    public static final String PREAUTH_BILLING_CYCLE_CODE = "BILLCYCLE_PREAUTH";

    public static final List<String> CHARGE_TYPE = Arrays.asList(
            ConstantesVelib.SUBSCRIPTION_REFUND_MANUAL_CHARGE_CODE,
            ConstantesVelib.REFUND_MANUAL_CHARGE_CODE,
            ConstantesVelib.SUBSCRIPTION_REFUND_MANUAL_CHARGE_PRODUCT,
            ConstantesVelib.REFUND_MANUAL_CHARGE_USAGE,
            ConstantesVelib.SUBSCRIPTION_REFUND_CHARGE_CODE,
            ConstantesVelib.SUBSCRIPTION_REFUND_CHARGE_PASS);

    // Refund subscription manual charges
    public static final String REFUND_MANUAL_CHARGE_CODE = "V1_CHARGE_MANUAL_REFUND";
    public static final String REFUND_MANUAL_CHARGE_USAGE = "V1_CHARGE_USAGE_MANUAL_REFUND";

    // Refund subscription charge code
    public static final String SUBSCRIPTION_REFUND_MANUAL_CHARGE_CODE = "V1_CHARGE_ABO_MANUAL_REFUND";
    public static final String SUBSCRIPTION_REFUND_CHARGE_CODE = "V1_CHARGE_ABO_REMBOURSEMENT";
    public static final String SUBSCRIPTION_REFUND_CHARGE_PASS = "V1_CHARGE_PASS_REMBOURSEMENT";

    // Refund subscription charge product
    public static final String SUBSCRIPTION_REFUND_CHARGE_PRODUCT = "V1_CHARGE_PRODUCT";
    public static final String SUBSCRIPTION_REFUND_MANUAL_CHARGE_PRODUCT = "V1_CHARGE_PRODUCT_MANUAL_REFUND";

    // Long duration offers' recurrent charges
    public static final String ANNUAL_SUBSCRIPTION_RECURRENT_CHARGE_CODE = "CH_V1_RE_ABO_ANNUEL";
    public static final String MONTHLY_SUBSCRIPTION_RECURRENT_CHARGE_CODE = "CH_V1_RE_ABO_MENSUEL";

    public static final String ANNUAL_SUBSCRIPTION = "annuel";
    public static final String MONTHLY_SUBSCRIPTION = "mensuel";

    // Actives conditions states
    public static final String JCD_ACTIVE_FILTER = "JOB_ACTIVE_FILTER";
    public static final String VELIB_ACTIVE_FILTER = "NEGATIVE_CHARGE_ACTIVE_FILTER";
    public static final String NEGATIVE_CHARGE_ACTIVE_FILTER = "NEGATIVE_CHARGE_ACTIVE_FILTER";
    public static final String MANUAL_REFUND_ACTIVE_FILTER = "MANUAL_REFUND_ACTIVE_FILTER";

    public static final List<String> CHARGE_CODES = Arrays.asList(
            new StringBuilder("8_").append(ConstantesVelib.ANNUAL_SUBSCRIPTION_RECURRENT_CHARGE_CODE).toString(),
            new StringBuilder("10_").append(ConstantesVelib.ANNUAL_SUBSCRIPTION_RECURRENT_CHARGE_CODE).toString(),
            new StringBuilder("12_").append(ConstantesVelib.ANNUAL_SUBSCRIPTION_RECURRENT_CHARGE_CODE).toString(),
            new StringBuilder("14_").append(ConstantesVelib.ANNUAL_SUBSCRIPTION_RECURRENT_CHARGE_CODE).toString());

    // Properties file name
    public static final String ANNUAIRE_SERVICE_PROPERTIES = "meveo-admin.properties";

    // Variable pour créer une ligne d'identification
    public static final String TEMPLATE_ENTITY_IDENTIFICATION = "Customeridentification";
    public static final String DESCRIPTION_IDENTIFICATION = "Customer identification access";
    public static final String DATE_FIN_VALIDITE_VAL = "9999-12-31T00:00:00+0200";
    public static final String GENERATE_CODE_SPLIT_CHARACTER = ":";
    public static final String CET_UNIQUE_CODE = "UniqueCode";
    public static final String UNIQUE_CODE8_RETRY_TIMES = "unique.code8.retry.times";

    // Name of Custom field
    public static final String NAME_CF_CUSTOMER = "customer_identification";
    public static final String DATE_DEBUT_VALIDITE = "Date_debut_validite_identification";
    public static final String DATE_FIN_VALIDITE = "Date_fin_validite_identification";
    public static final String STATUT = "statut_identification";
    public static final String ID = "id_identification";
    public static final String TYPE = "type_identification";
    public static final String STATUT_DATE = "statut_date_identification";
    public static final String CODE_UNIQUE = "Code_Unique";
    public static final String ORIGIN_ID = "origin_identification";
    public static final String JSON_REQUEST_BODY = "CF_JSON_REQUEST_BODY";
    public static final String CF_OFFER_UPGRADE_PENDING_CODE = "CF_OFFER_UPGRADE_PENDING_REQUEST";
    public static final String CA_OFFER_UPGRADES = "CF_CA_OFFER_UPGRADES";

    // ingenicoPaymentResponse field in json order request
    public static final String INGENICO_AMOUNT_FIELD = "ingenicoAmount";
    public static final String INGENICO_PAYMENT_ID_FIELD = "ingenicoPayId";
    public static final String INGENICO_STATUS_FIELD = "ingenicoStatus";
    public static final String INGENICO_PRE_ATHORISATION_VALUE = "5";

    // Message API
    public static final String ACTIF_CUSTOMER_NOT_FOUND = "No Actif Customer Account found"; // voir pour deplacer dans ErrorCodesEnum
    public static final String INEXISTENT_CUSTOMER_ACCOUNT = "Customer Account not found";
    public static final String CODE_EXIST = "Can not generate a code";
    public static final String DATE_FORMAT = "ERROR FORMAT DATE";
    public static final String CODE_GENERATED = "the generated code is : ";
    public static final String FAILD_EXECUTE_API = "Failed to execute API";

    // Custom entity code
    public static final String CE_GENERAL_TERMS = "GENERAL_TERMS_MANAGEMENT";
    public static final String CE_PRODUCT_CARD_PROVISIONNING = "PRODUCT_CARD_PROVISIONNING";

    // Custom entity CF code
    public static final String CF_ID_TOTEM = "CF_ID_TOTEM";
    public static final String CF_EFFECTIVE_DATE = "EFFECTIVE_DATE";
    public static final String CF_SUSPENSION_DATE = "SUBSCRIPTION_SUSPENSION_DATE";
    public static final String CUSTOMER_CODE = "CUSTOMER_CODE";
    public static final String CARD_QUANTITY = "CARD_QUANTITY";
    public static final String FIST_CARD_TAG = "FIST_CARD_TAG";
    public static final String SUSBCRIPTION_OFFER_CODE = "SUSBCRIPTION_OFFER_CODE";
    public static final String SUSBCRIPTION_OFFER_DESCRIPTION = "SUSBCRIPTION_OFFER_DESCRIPTION";
    public static final String CREATED_DATE = "CREATED_DATE";

    // Paragon dédié à Paris (externaliser dans custom entity)
    public static final String PROVISIONNING_STATUS_NEW = "NEW";
    public static final String PROVISIONNING_STATUS_AR = "AR";
    public static final String PROVISIONNING_STATUS_AF = "AF";
    public static final String PROVISIONNING_STATUS_NPAI = "NPAI";

    // Provisioning card entity
    public static final String CE_PROVISIONING_CARD_CODE = "PRODUCT_CARD_PROVISIONNING";// PARAGON (propre à paris)
    public static final String CF_UPDATED_DATE_CODE = "UPDATED_DATE";
    public static final String CF_CUSTOMER_CODE_CODE = "CUSTOMER_CODE";
    public static final String CF_PROVISIONNING_STATUS_CODE = "PROVISIONNING_STATUS";// PARAGON (propre à paris)
    public static final String CF_CARD_RESPONSE_DESCRIPTION_CODE = "CARD_RESPONSE_DESCRIPTION";
    public static final String CF_CARD_RESPONSE_UID_CODE = "CARD_RESPONSE_UID";

    // Custom fields codes
    public static final String V1_MAX_QUANTITY_BEFORE_PRICE_FREEZE = "V1_MAX_QUANTITY_BEFORE_PRICE_FREEZE";
    public static final String V1_ALLOW_BONUS_BENEFIT = "V1_ALLOW_BONUS_BENEFIT";
    public static final String CF_EMAIL_CODE = "CF_CA_EMAIL";
    public static final String CF_LOGIN_CODE = "CF_CA_LOGIN";
    public static final String CF_PASSWORD_CODE = "CF_MOT_DE_PASSE";
    public static final String CF_PIN_CODE_CODE = "CF_CODE_PIN";
    public static final String CF_BIRTHDATE_CODE = "CF_BIRTHDATE";
    public static final String CF_AUTORISATION_VELO_CODE = "CF_AUTORISATION_VELO";
    public static final String CF_MAIL_VALIDATION_CODE = "CF_MAIL_VALIDATION";
    public static final String CF_PHONE_VALIDATION_CODE = "CF_PHONE_VALIDATION";
    public static final String CF_DOC_VALIDATION_CODE = "CF_DOC_VALIDATION";
    public static final String CF_ID_VALIDATION_CODE = "CF_ID_VALIDATION";
    public static final String CF_RIB_VALIDATION_CODE = "CF_RIB_VALIDATION";
    public static final String CF_AUTO_SUBSCRIPTION_RENEWAL = "CF_AUTO_SUBSCRIPTION_RENEWAL";
    public static final String CF_TEMPORARY_PASS_CODE = "CF_TEMPORARY_PASSWORD_TAG";
    public static final String CF_NIVEAU_ASSISTANCE_CODE = "CF_NIVEAU_ASSISTANCE";
    public static final String CF_VITESSE_MAX_CODE = "CF_VITESSE_MAX";
    public static final String CF_KM_TOTAL_CODE = "CF_KM_TOTAL";
    public static final String CF_KM_ELECTRIQUE_CODE = "CF_KM_ELECTRIQUE";
    public static final String CF_TRIP_COUNTER_IN_MONTH_CODE = "CF_TRIP_COUNTER_IN_MONTH";
    public static final String CF_TRIPS_AVERAGE_DURATION_IN_MONTH_CODE = "CF_TRIPS_AVERAGE_DURATION_IN_MONTH";
    public static final String CF_TRIPS_HIGHEST_DISTANCE_IN_MONTH_CODE = "CF_TRIPS_HIGHEST_DISTANCE_IN_MONTH";
    public static final String CF_CLIENT_PROFILE_UPDATE_DATE_CODE = "CF_CLIENT_PROFILE_UPDATE_DATE";
    public static final String CF_PROFILE_UPDATE_ORIGIN_CODE = "CF_PROFILE_UPDATE_ORIGIN";
    public static final String CF_DURATIONS_MAP_CODE = "V1_MATRICE_TAILLE_PALIER";
    public static final String CF_PRICES_MAP_CODE = "V1_MATRICE_PRIX_PALIER";
    public static final String CF_LAST_TIME_SLOT = "CF_LAST_TIME_SLOT";
    public static final String CF_USAGE_NFC = "CF_USAGE_NFC";
    public static final String CF_CGAU = "CF_CGAU";
    public static final String CF_PREFERRED_CARD = "CF_PREFERRED_CARD";
    public static final String CF_SUSPENSION_DETAILS = "CF_SUSPENSION_DETAILS";
    public static final String CF_VELIB_CONTACT_SMS = "CF_VELIB_CONTACT_SMS";
    public static final String CF_VELIB_CONTACT_MAIL = "CF_VELIB_CONTACT_MAIL";
    public static final String CF_PARTENAIRE_CONTACT_MAIL = "CF_PARTENAIRE_CONTACT_MAIL";
    public static final String CF_PARTENAIRE_CONTACT_SMS = "CF_PARTENAIRE_CONTACT_SMS";
    public static final String CF_MAIL_PROOF_OF_PAYMENT = "CF_MAIL_PROOF_OF_PAYMENT";
    public static final String CF_MARKETING_CONTACT_MAIL = "CF_MARKETING_CONTACT_MAIL";
    public static final String CF_MARKETING_CONTACT_SMS = "CF_MARKETING_CONTACT_SMS";
    public static final String CF_PRE_APPROVAL_BANK_MAP = "V1_PRE_APPROVAL_BANK_MAP";
    public static final String CF_PROVIDER_MULTIVILLE_NOTIF = "CF_PROVIDER_MULTIVILLE_NOTIF";
    public static final String CF_TREATMENT_MAIL_PROOF_OF_PAYMENT = "CF_TREATMENT_MAIL_PROOF_OF_PAYMENT";
    public static final String CF_CONTROLE_APPLY_TERMINATION_CHARGE = "V1_CHECKBOX_CONTROLE_APPLY_TERMINATION_CHARGE";
    public static final String CF_STATUS_TREATEMENT_RETAKE_BONUS = "CF_STATUS_TREATEMENT_RETAKE_BONUS";
    public static final String CF_INGENICO_RESULT_PAYMENT_STATUS = "CF_INGENICO_RESULT_PAYMENT_STATUS";
    public static final String CF_INGENICO_RESULT_PAYMENT_MESSAGE = "CF_INGENICO_RESULT_PAYMENT_MESSAGE";

    // Bonus
    public static final String CF_CUSTOMER_BONUS_DETAILS = "CF_CUSTOMER_BONUS_DETAILS";
    public static final String CF_BONUS_DETAILS = "CF_CUSTOMER_BONUS_DETAILS";
    public static final String BONUS_MAP_KEY_VAL = "BONUS_DATE/BONUS_TYPE";

    // Coupon

    public static final String CF_COUPON_TEMPLATE = "couponTemplate";
    public static final String CF_COUPON_INSTANCE = "CouponInstance";
    public static final String CF_COUPON_REMAINING_QUANTITY = "remainingQuantity";
    public static final String CF_COUPON_VALID_FROM = "validFrom";
    public static final String CF_COUPON_VALID_TO = "validTo";
    public static final String CF_COUPON_STATUS = "status";
    public static final String CF_COUPON_OFFERS = "offers";
    public static final String CF_COUPON_DISCOUNT_TYPE = "discountType";
    public static final String CF_COUPON_DISCOUNT_VALUE = "discountValue";
    public static final String CF_COUPON_DURATION = "duration";
    public static final String CF_COUPON_TYPE = "couponType";
    public static final String CF_DESCRIPTION_I18N = "descriptionI18n";
    public static final String DISCOUNT_TYPE_PERCENT = "percent";
    public static final String DISCOUNT_TYPE_AMOUNT = "amount";

    // Subscription
    public static final String CF_SUB_COUPON = "CF_COUPON_REFERENCE";
    public static final String CF_SUB_REMAINING_USAGES = "CF_REMAINING_USAGES";
    public static final String SUBSCRIPTION_DATE = "subscriptionDate";

    // Parameter3 Wallet Operation
    public static final String WO_PARAMETER3_SEPARATOR = ";";
    public static final String WO_PARAMETER3_SUB_SEPARATOR = ":";
    public static final String WO_PARAMETER3_BILLED_QUANTITY = "BILLED_QUANTITY:";
    public static final String WO_PARAMETER3_OVERCHARGING_QUANTITY = "OVERCHARGING_QUANTITY:";
    public static final String WO_PARAMETER3_USED_BONUS = "USED_BONUS";
    public static final String WO_PARAMETER3_LEFT_BONUS = "LEFT_BONUS";
    public static final String WO_PARAMETER3_BONUS_EARNED = "BONUS_EARNED";
    //public static final String WO_PARAMETER3_CURRENT_BONUS = "CURRENT_BONUS";

    // Parameter3 EDR
    public static final String EDR_PARAMETER3_SEPARATOR = ";";
    public static final String EDR_PARAMETER3_DISTANCE = "DISTANCE";
    public static final String EDR_PARAMETER3_BONUS_EARNED = "BONUS_EARNED:";
    //public static final String EDR_PARAMETER3_CURRENT_BONUS = "CURRENT_BONUS:";
    
    // Parameter4 EDR
    //public static final String EDR_PARAMETER4_SEPARATOR = ";";
    //public static final String EDR_PARAMETER4_SUB_SEPARATOR = ":";
    //public static final String EDR_PARAMETER4_STATUT_DEPART = "STATUTDEPART";
    //public static final String EDR_PARAMETER4_STATUT_ARRIVEE = "STATUTARRIVEE";

    // Language tags
    public static final String LANGUAGE_FRA = "FRA";
    public static final String LANGUAGE_ENG = "ENG";
    public static final String LANGUAGE_SPA = "SPA";

    // Propre à Paris
    public static final String VELIB = "velib";
    public static final String NAVIGO = "navigo";
    public static final String LANGUAGE_FRA_VELIB = "Carte Velib";
    public static final String LANGUAGE_ENG_VELIB = "Velib Card";
    public static final String LANGUAGE_SPA_VELIB = "Tarjeta Velib";
    public static final String LANGUAGE_FRA_NAVIGO = "Pass Navigo";
    public static final String LANGUAGE_ENG_NAVIGO = "Navigo Pass";
    public static final String LANGUAGE_SPA_NAVIGO = "Pass Navigo";
    public static final String VELIB_RECURRING = "VELIB_RECURRING";
    public static final String CF_LAST_DATE_EXPORT_AO = "CF_LAST_DATE_EXPORT_AO";
    public static final String VELIB_ONESHOT = "VELIB_ONESHOT";
    public static final String INVOICE_TYPE_F = "F";
    public static final String CF_EMAIL_RECIPIENTS = "CF_EMAIL_RECIPIENTS";

    // CF Multiville
    public static final String CF_PROVIDER_MULTIVILLE_VERSIONNING = "CF_PROVIDER_MULTIVILLE_VERSIONNING";

    // CF CustomerDeviceDetails
    public static final String CF_DEVICE_OPERATING_SYSTEM = "CF_DEVICE_OPERATING_SYSTEM";
    public static final String CF_DEVICE_TOKEN_VALUE = "CF_DEVICE_TOKEN_VALUE";

    // CF Instance
    public static final String CF_INSTANCE_VALUE = "CF_INSTANCE_VALUE";

    // CF Suspension Matrix
    public static final String LITIGE_SYNDICAT = "litige_syndicat";
    public static final String LITIGE_SYNDICAT_VAL_YES = "yes";
    public static final String LITIGE_SYNDICAT_VAL_KEY_VAL = "SUSPENSION_DATE/SUSPENSION_TYPE";

    // Services codes
    public static final String SERVICE_CHARGE_ONE_SHOT = "V1_SERVICE_CHARGE_ONE_SHOT";
    public static final String SERVICE_ABONNEMENT = "V1_SERVICE_ABONNEMENT";
    public static final String NUMBER_OF_VALID_DAYS = "V1_NUMBER_OF_VALID_DAYS";
    public static final String TRANSMITTER_SYNC = "V1_TRANSMITTER_SYNCHRONIZATION";
    public static final String V1_VITESSE_MAX_AUTORISEE = "V1_VITESSE_MAX_AUTORISEE";
    public static final String V1_NIVEAU_ASSISTANCE = "V1_NIVEAU_ASSISTANCE";
    public static final String V1_SERVICE_OPTION_VITESSE_MAX_AUTORISEE = "V1_SERVICE_OPTION_VITESSE_MAX_AUTORISEE";
    public static final String V1_SERVICE_OPTION_NIVEAU_ASSISTANCE = "V1_SERVICE_OPTION_NIVEAU_ASSISTANCE";
    public static final String V1_NB_TRAJETS = "V1_NB_TRAJETS";
    public static final String V1_NBR_TRAJET_RESTANT = "V1_NBR_TRAJET_RESTANT";
    public static final String V1_SERVICE_OPTION_OVERFLOW = "V1_SERVICE_OPTION_OVERFLOW";
    public static final String V1_ACTIVATION_BIKE_AUTHORIZATION = "V1_ACTIVATION_BIKE_AUTHORIZATION";
    public static final String V1_ACTIVATION_DROIT_OVERFLOW = "V1_ACTIVATION_DROIT_OVERFLOW";
    public static final String SERVICE_OPTION_TRANSMITTERSYNC = "V1_SERVICE_OPTION_TRANSMITTERSYNC";
    public static final String V1_SERVICE_OPTION_BIKE_AUTHORIZATION = "V1_SERVICE_OPTION_BIKE_AUTHORIZATION";
    public static final String V1_SERVICE_OPTION_VELO_PRIS_SIMULTANEMENT = "V1_SERVICE_OPTION_VELO_PRIS_SIMULTANEMENT";
    public static final String V1_NB_VELO_PRIS_SIMULTANEMENT = "V1_NB_VELO_PRIS_SIMULTANEMENT";

    // Origin variables
    public static final String TOTEM = "TOTEM";
    public static final String MOBA = "MOBA";
    public static final String MOBI = "MOBI";
    public static final String WEB = "WEB";
    public static final String MIGRATION = "MIGRATION";

    // Status stations usages
    public static final String FULL = "FULL";
    public static final String NOTFULL = "NOTFULL";
    public static final String EMPTY = "EMPTY";

    // Variables for ResetPassword
    public static final String CF_CUSTOMER_RESET_PASSWORD = "CF_CUSTOMER_RESET_PASSWORD";
    public static final int CF_CUSTOMER_RESET_PASSWORD_LENGTH = 8;
    public static final String CF_TEMPORARY_PASSWORD_TAG = "CF_TEMPORARY_PASSWORD_TAG";
    public static final String CF_LAST_DATE_UPDATE_PASSWORD = "CF_LAST_DATE_UPDATE_PASSWORD";

    // Yes-No tags
    public static final String YES_VALUE = "yes";
    public static final String NO_VALUE = "no";
    public static final String CF_Y_VALUE = "Y";
    public static final String CF_N_VALUE = "N";
    public static final String CF_YES_VALUE = "YES";
    public static final String CF_NO_VALUE = "NO";

    // Matrix
    public static final String PIPE_SEPARATOR = "|";
    public static final String MATRIX_KEY = "key";

    // Suspension CF values
    public static final String PRODUCT_CARD_VELIB = "PRODUCT_CARD_VELIB";
    public static final String SUSPENSION_MAP_KEY_VAL = "SUSPENSION_DATE/SUSPENSION_TYPE";
    public static final String BLOCAGE_MISE_EN_SERVICE = "blocage_mise_en_service";

    // Bikes types
    public static final String VM_BIKE_ID = "VM";
    public static final String VAE_BIKE_ID = "VAE";

    // Preapproval max amount ref in CF Map
    public static final String PRE_APPROVAL_BANK_MAP_MAX_REF = "+";

    // Salesforce contract
    public static final String CF_CONTRACT_SALESFORCE_ID_CODE = "CF_CONTRACT_SALESFORCE_ID";// propre à paris

    // Salesforce account
    public static final String CF_CA_SALESFORCE_ID_CODE = "CF_CA_SALESFORCE_ID";// propre à paris
    public static final String CF_DEVICE_TOKEN_VALUE_CODE = "CF_DEVICE_TOKEN_VALUE";
    public static final String CF_DEVICE_OPERATING_SYSTEM_CODE = "CF_DEVICE_OPERATING_SYSTEM";

    // MAIL
    public static final String TEMPLATE_EMAILERROR_ERROR_NAME = "EMAIL-ERROR";
    public static final String TEMPLATE_EMAILREPORT_REPORT_NAME = "EMAIL-REPORT";
    public static final String TEMPLATE_EMAILPROOF_PAYMENT_NAME = "EMAIL-JUSTIFPAIEMENT";
    public static final String TEMPLATE_EMAILPROOF_PAYMENT_ANONYME_NAME = "EMAIL-JUSTIFPAIEMENT-ANONYME";

    // SMS
    public static final String TEMPLATE_NAME = "SMS-";
    public static final String EVENT0001 = "EVENT0001";
    public static final String PARAM_MINUTES = "nbMinutes";
    public static final String HASH_TAG = "#";

    // Boss
    public static final String CF_AUTHORIZATION_FORBIDDEN_CODE = "CF_AUTHORIZATION_FORBIDDEN";
    public static final String CF_AUTHORIZATION_PINCODE_CODE = "CF_AUTHORIZATION_PINCODE";
    public static final String CF_AUTHORIZATION_EBIKESPEEDMAX_CODE = "CF_AUTHORIZATION_EBIKESPEEDMAX";
    public static final String CF_AUTHORIZATION_EBIKELEVEL_CODE = "CF_AUTHORIZATION_EBIKELEVEL";
    public static final String CF_AUTHORIZATION_ACCESSID_CODE = "CF_AUTHORIZATION_ACCESSID";
    public static final String CF_AUTHORIZATION_ACCESSTYPE_CODE = "CF_AUTHORIZATION_ACCESSTYPE";

    // Boss and Salesforce Notifications
    public static final String CODE = "code";
    public static final String CARTE = "carte";
    public static final String SIGNED = "signed";
    public static final String CGAU_NON_SIGNEES = "cgau_non_signees";
    public static final String BOSS_AUTHORIZATION_FORBIDDEN = "boss_authorization_forbidden";
    public static final String STATUS_CODE = "statusCode";
    public static final String RESPONSE = "response";
    public static final String MIGRATION_CREDITCATEGORY = "VELIB1_ABONNE";

    // Salesforce object
    public static final String CUSTOMER_ID = "customerAccountID";
    public static final String SALESFORCE_ID = "salesforceID"; // propre a paris
    public static final String SUBSCRIPTION_ID = "subscriptionID";
    public static final String DATA = "data";
    public static final String URI = "uri";
    public static final String ACCOUNT_ID_FIELD_NAME = "AccountId__c";
    public static final String CALL_CREATE_CONTRAT_SALESFORCE_SCRIPT = "CALL_CREATE_CONTRAT_SALESFORCE_SCRIPT";

    // Resiliation
    public static final String MOYEN_PAIEMENT_INVALIDE = "moyen_paiement_invalide";
    public static final String SUBSCRIPTION_TERMINATION_ACTION = "SUBSCRIPTION_TERMINATION_ACTION";
    public static final String TR_CLIENT_MISCONDUCT = "TR_CLIENT_MISCONDUCT";

    // APPLIES_TO
    public static final String APPLIES_TO_SUB = "SUB";

    // Custom fields associated to the custom entity 'offer upgrade request'
    public static final String CF_OFFER_CODE_CODE = "OFFER_CODE";
    public static final String CF_OFFER_UPGRADE_DATE_CODE = "OFFER_UPGRADE_DATE";
    public static final String CF_ORIGIN_CODE = "ORIGIN";
    public static final String CF_REQUEST_ID_CODE = "REQUEST_ID";
    public static final String CF_OFFER_UPGRADE_ENTITY_CODE = "OFFER_UPGRADE_ENTITY";
    public static final String CF_ORIGIN_ID_CODE = "ORIGIN_ID";
    public static final String CF_CREATED_DATE_CODE = "CREATED_DATE";
    public static final String CF_CUSTOMER_CODE = "CUSTOMER_CODE";
    public static final String TR_OFFER_UPDATE = "TR_OFFER_UPDATE";
    public static final String CF_QUOTATION_ID = "QUOTATION_ID";

    // Custom fields associated to the custom entity 'PRE_AUTH_INGENICO_STATUS_FILE'
    public static final String CF_PRE_AUTH_INGENICO_STATUS_FILE = "PRE_AUTH_INGENICO_STATUS_FILE";
    public static final String CF_PRE_AUTH_OPENCELL_PAYMENTS_DATE = "OPENCELL_PAYMENTS_DATE";
    public static final String CF_PRE_AUTH_OPENCELL_PAYMENTS_TO_BE_PROCESSED_COUNT = "OPENCELL_PAYMENTS_TO_BE_PROCESSED_COUNT";
    public static final String CF_PRE_AUTH_OPENCELL_PAYMENTS_PROCESSED_COUNT = "OPENCELL_PAYMENTS_PROCESSED_COUNT";
    public static final String CF_PRE_AUTH_INGENICO_SUBMISSION_DATE = "INGENICO_SUBMISSION_DATE";
    public static final String CF_PRE_AUTH_INGENICO_SUBMISSION_STATUS = "INGENICO_SUBMISSION_STATUS";
    public static final String CF_PRE_AUTH_INGENICO_PAYMENTS_CALLBACK_DATE = "INGENICO_PAYMENTS_CALLBACK_DATE";
    public static final String CF_PRE_AUTH_INGENICO_PAYMENTS_CALLBACK_ACCEPTED_COUNT = "INGENICO_PAYMENTS_CALLBACK_ACCEPTED_COUNT";
    public static final String CF_PRE_AUTH_INGENICO_PAYMENTS_CALLBACK_REJECTED_COUNT = "INGENICO_PAYMENTS_CALLBACK_REJECTED_COUNT";

    // Payment Parameter
    public static final String RG_CARD = "RG_CARD"; // Valid payment card
    public static final String IP_CARD = "IP_CARD"; // Reject payment card
    public static final String P_VALUE = "P"; // Type of payment
    public static final String ACCT_CA_VALUE = "ACCT_CA"; // Customer Account
    public static final String CREDITCATEGORY_ANONYME = "ANONYME"; // Anonyme Customer
    public static final String PROOF_STATUS_TOTREAT = "TOTREAT";
    public static final String PROOF_STATUS_TREAT = "TREAT";
    public static final String PROOF_STATUS_ERROR = "ERROR";
    public static final String INGENICO_PAYMENT_STATUS_REJECTED = "REJECTED";
    public static final String INGENICO_PAYMENT_STATUS_ACCEPTED = "ACCEPTED";
    public static final String INGENICO_PAYMENT_STATUS_SENT = "SENT";

    // Subscription status
    public static final String SUSPENDED = "SUSPENDED";
    public static final String TERMINATE = "TERMINATE";
    public static final String ACTIVE = "ACTIVE";

    // Default values
    public static final int MAX_BIKE_DEFAULT_VALUE = 0;
    public static final double EBIKE_LEVEL_DEFAULT_VALUE = 0;
    public static final double EBIKE_SPEED_MAX_DEFAULT_VALUE = 0;
    public static final String TRANSMITTER_SYNC_DEFAULT_VALUE = "no";
    public static final String OVERFLOW_DEFAULT_VALUE = "no";
    public static final String EBIKE_DEFAULT_VALUE = "no";
    
    public static final String COUPON_INSTANCE = "CouponInstance";
    public static final String COUPON_TYPE_GENERIC = "generic";
    public static final String CF_REMAINING_QTE = "remainingQuantity";

    public enum statutCarte {
        Actif,
        Inactif
    }
    
    public static final int NEXT_INVOICE_DATE_DELAY = 7;
    public static final String CAL_DEFAULT_16DAYS = "CAL_DEFAULT_16DAYS";
}
