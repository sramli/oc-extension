package org.meveo.common;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonValue;
import com.google.common.collect.ImmutableMap;

public enum CouponStatusEnum {

    DRAFT("draft") , ACTIVE("active"), CANCELED("canceled");

    CouponStatusEnum(String value) {
       this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    private String value;
    public static final Map<String,CouponStatusEnum> instances = ImmutableMap.of("draft", DRAFT, "active", ACTIVE, "canceled", CANCELED);

}
