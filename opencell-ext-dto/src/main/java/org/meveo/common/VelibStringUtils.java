package org.meveo.common;

import org.apache.commons.lang3.RandomStringUtils;

public class VelibStringUtils {

    /**
     * Using UUID would be better , more accurate and highest guarantee of unicity, but it's on 40 character !
     * Thus we opted for currentTimeMillis + random characters to fit 16 as limited length
     */
    public static String generateUniqueKey() {
        String timeKey = String.valueOf(System.currentTimeMillis());
        return timeKey + RandomStringUtils.randomAlphanumeric(16 - timeKey.length());
    }
}
