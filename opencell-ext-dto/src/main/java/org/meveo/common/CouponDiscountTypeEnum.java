package org.meveo.common;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonValue;
import com.google.common.collect.ImmutableMap;

public enum CouponDiscountTypeEnum {

    AMOUNT("amount") , PERCENT("percent");

    CouponDiscountTypeEnum(String value) {
       this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    private String value;
    public static final Map<String,CouponDiscountTypeEnum> instances = ImmutableMap.of("amount", AMOUNT, "percent", PERCENT);

    public static CouponDiscountTypeEnum valueOfCustom(String value) {
        for (CouponDiscountTypeEnum couponDiscountTypeEnum : CouponDiscountTypeEnum.values()) {
            if(couponDiscountTypeEnum.equals(value) ||couponDiscountTypeEnum.getValue().equals(value)) {
                return couponDiscountTypeEnum;
            }
        }
        return null;
    }
}
