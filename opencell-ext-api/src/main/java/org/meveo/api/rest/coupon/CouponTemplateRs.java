package org.meveo.api.rest.coupon;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.meveo.dto.coupon.CustomEntityInstancesDto;
import org.meveo.api.dto.ActionStatus;
import org.meveo.api.rest.IBaseRs;
import org.meveo.common.VelibPagingAndFiltering;
import org.meveo.dto.coupon.CouponTemplateDto;

/**
 * A REST API handling CouponTemplate
 */
@Path("/velib/V1/coupon/template")
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface CouponTemplateRs extends IBaseRs {

    /**
     * Create a CouponTemplate
     * @param postData
     * @return Request processing status
     */
    @POST
    @Path("/")
    ActionStatus create(CouponTemplateDto postData);

    /**
     * Update a CouponTemplate
     * @param postData
     * @return Request processing status
     */
    @PUT
    @Path("/")
    ActionStatus update(CouponTemplateDto postData);

    /**
     * Create (if not exist) or Update a CouponTemplate
     * @param subscriptionDto
     * @return Request processing status
     */
    @POST
    @Path("/createOrUpdate")
    ActionStatus createOrUpdate(CouponTemplateDto subscriptionDto);


    /**
     * List CouponTemplate operations matching a given criteria
     *
     * @param pagingAndFiltering Pagination and filtering criteria
     * @return List of wallet operations
     */
    @POST
    @Path("/list")
    CustomEntityInstancesDto list(VelibPagingAndFiltering pagingAndFiltering);

}
