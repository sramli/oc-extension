package org.meveo.api.rest.coupon.impl;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.meveo.api.coupon.CouponTemplateApi;
import org.meveo.dto.coupon.CustomEntityInstancesDto;
import org.meveo.api.dto.ActionStatus;
import org.meveo.api.dto.ActionStatusEnum;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.coupon.CouponTemplateRs;
import org.meveo.api.rest.impl.BaseRs;
import org.meveo.common.VelibPagingAndFiltering;
import org.meveo.dto.coupon.CouponTemplateDto;

@RequestScoped
@Interceptors({ WsRestApiInterceptor.class })
public class CouponTemplateRsImpl extends BaseRs implements CouponTemplateRs {

    @Inject
    private CouponTemplateApi couponTemplateApi;

    @Override
    public ActionStatus create(CouponTemplateDto postData) {
        ActionStatus result = new ActionStatus(ActionStatusEnum.SUCCESS, "");

        try {
            couponTemplateApi.create(postData);
        } catch (Exception e) {
            processException(e, result);
        }

        return result;
    }

    @Override
    public ActionStatus update(CouponTemplateDto postData) {
        ActionStatus result = new ActionStatus(ActionStatusEnum.SUCCESS, "");

        try {
            couponTemplateApi.update(postData);
        } catch (Exception e) {
            processException(e, result);
        }

        return result;
    }

    @Override
    public ActionStatus createOrUpdate(CouponTemplateDto postData) {
        ActionStatus result = new ActionStatus(ActionStatusEnum.SUCCESS, "");

        try {
            couponTemplateApi.createOrUpdate(postData);
        } catch (Exception e) {
            processException(e, result);
        }

        return result;
    }

    @Override
    public CustomEntityInstancesDto list(VelibPagingAndFiltering pagingAndFiltering) {
        CustomEntityInstancesDto result = new CustomEntityInstancesDto();

        try {
            result = couponTemplateApi.list(pagingAndFiltering);
        } catch (Exception e) {
            processException(e, result.getActionStatus());
        }
        return result;
    }
}
