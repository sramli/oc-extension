package org.meveo.api.rest.coupon.impl;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.meveo.api.coupon.CouponInstanceApi;
import org.meveo.api.dto.ActionStatus;
import org.meveo.api.dto.ActionStatusEnum;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.coupon.CouponInstanceRs;
import org.meveo.api.rest.impl.BaseRs;
import org.meveo.common.VelibApiErrorCodeEnum;
import org.meveo.common.VelibPagingAndFiltering;
import org.meveo.dto.coupon.CouponInstanceRequestDto;
import org.meveo.dto.coupon.CouponInstancesResponseDto;
import org.slf4j.Logger;

@RequestScoped
@Interceptors({ WsRestApiInterceptor.class })
public class CouponInstanceRsImpl extends BaseRs implements CouponInstanceRs {

    @Inject
    private CouponInstanceApi couponInstanceApi;

    @Inject
	private Logger log;

    @Override
    public ActionStatus generate(CouponInstanceRequestDto postData) {

        ActionStatus result = new ActionStatus(ActionStatusEnum.SUCCESS, "");
        try {
			couponInstanceApi.generate(postData);
        } catch (Exception e) {
            processException(e, result);
        }
        return result;
    }

	@Override
	public ActionStatus isCouponValid(String couponCode, String subscriptionDate, String offerCode) {
		ActionStatus result = new ActionStatus(ActionStatusEnum.SUCCESS, "");
		try {
			if (!couponInstanceApi.isCouponValid(couponCode, subscriptionDate, offerCode)) {
				result.setStatus(ActionStatusEnum.FAIL);
				result.setErrorCode(VelibApiErrorCodeEnum.ERR156);
				result.setMessage(VelibApiErrorCodeEnum.ERR156.getDescription());
			}
		} catch (Exception e) {
			if(e.getMessage().equals(VelibApiErrorCodeEnum.ERR156.getDescription())
			|| e.getMessage().equals(VelibApiErrorCodeEnum.ERR159.getDescription())) {

				VelibApiErrorCodeEnum errorCode;

				if(e.getMessage().equals(VelibApiErrorCodeEnum.ERR156.getDescription())) {
					errorCode = VelibApiErrorCodeEnum.ERR156;
				} else {
					errorCode = VelibApiErrorCodeEnum.ERR159;
				}
				result.setStatus(ActionStatusEnum.FAIL);
				result.setErrorCode(errorCode);
				result.setMessage(errorCode.getDescription());
			}
			processException(e, result);
		}
		return result;
	}

	/**
	 * List CouponInstances matching a given criteria
	 *
	 * @param pagingAndFiltering Pagination and filtering criteria
	 * @return List of CouponInstances
	 */
	@Override
	public CouponInstancesResponseDto list(VelibPagingAndFiltering pagingAndFiltering) {
		CouponInstancesResponseDto result = new CouponInstancesResponseDto();

		try {
			return couponInstanceApi.list(pagingAndFiltering);
		} catch (Exception e) {
			processException(e, result.getActionStatus());
		}

		return result;
	}
}
