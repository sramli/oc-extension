package org.meveo.api.rest.coupon;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.meveo.api.dto.ActionStatus;
import org.meveo.api.rest.IBaseRs;
import org.meveo.common.VelibPagingAndFiltering;
import org.meveo.dto.coupon.CouponInstanceRequestDto;
import org.meveo.dto.coupon.CouponInstancesResponseDto;

/**
 * A REST API handling CouponInstance
 */
@Path("/velib/V1/coupon/instance")
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface CouponInstanceRs extends IBaseRs {

    /**
     * Generating the CouponInstance(s) according to the requested CouponTemplate.
     * @param postData
     * @return Request processing status
     */
    @POST
    @Path("/")
    ActionStatus generate(CouponInstanceRequestDto postData);

	/**
	 * Says whether a coupon is valid or not. Subscription date and offer code are
	 * optional. If present, the validity of the coupon is checked against these two
	 * parameters.
	 * 
	 * @param couponCode       the coupon code
	 * @param subscriptionDate the subscription date
	 * @param offerCode        the offer code
	 * @return
	 */
	@GET
	@Path("/validation")
    ActionStatus isCouponValid(@QueryParam("couponCode") String couponCode, @QueryParam("subscriptionDate") String subscriptionDate, @QueryParam("offerCode") String offerCode);


	/**
	 * List CouponInstances matching a given criteria
	 *
	 * @param pagingAndFiltering Pagination and filtering criteria
	 * @return List of CouponInstances
	 */
	@POST
	@Path("/list")
	CouponInstancesResponseDto list(VelibPagingAndFiltering pagingAndFiltering);
}
