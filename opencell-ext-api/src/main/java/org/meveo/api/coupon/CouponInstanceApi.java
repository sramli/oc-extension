package org.meveo.api.coupon;

import static org.meveo.common.VelibPagingAndFiltering.CF_CRETERIA_OPERATION_MAP;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.BaseApi;
import org.meveo.api.CustomEntityInstanceApi;
import org.meveo.api.MeveoApiErrorCodeEnum;
import org.meveo.api.exception.BusinessApiException;
import org.meveo.api.exception.EntityDoesNotExistsException;
import org.meveo.api.exception.InvalidParameterException;
import org.meveo.api.exception.MeveoApiException;
import org.meveo.api.exception.MissingParameterException;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.common.ConstantesVelib;
import org.meveo.common.CouponStatusEnum;
import org.meveo.common.CouponTypeEnum;
import org.meveo.common.VelibApiErrorCodeEnum;
import org.meveo.common.VelibPagingAndFiltering;
import org.meveo.dto.coupon.CouponInstanceDto;
import org.meveo.dto.coupon.CouponInstanceRequestDto;
import org.meveo.dto.coupon.CouponInstancesResponseDto;
import org.meveo.dto.coupon.CouponTemplateDto;
import org.meveo.exception.VelibApiException;
import org.meveo.model.billing.Subscription;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.crm.EntityReferenceWrapper;
import org.meveo.model.crm.custom.CustomFieldValues;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.meveo.model.customEntities.CustomEntityTemplate;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.cardrequest.CustomEntityServiceVelib;
import org.meveo.service.coupon.CouponVelibService;
import org.meveo.service.custom.CustomEntityInstanceService;
import org.meveo.service.offer.OfferVelibTemplateService;
import org.primefaces.model.SortOrder;


@Stateless
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class CouponInstanceApi extends BaseApi {

    @Inject
    private CustomEntityInstanceService customEntityInstanceService;
    
    @Inject
    private CustomEntityServiceVelib customEntityServiceVelib;

    @Inject
    private CustomEntityInstanceApi customEntityInstanceApi;
    
    @Inject
	private CouponVelibService couponVelibService;
	
	@Inject
    private OfferVelibTemplateService offerVelibTemplateService;

    @Inject
    private SubscriptionService subscriptionService;
    
    public void generate(CouponInstanceRequestDto postData) throws MeveoApiException, BusinessException {

        log.debug("Validating Compagne Coupon ...");
        CustomEntityInstance compagneCoupon = this.validateCompagneCoupon(postData);

        log.debug("Generating Coupon Instances ...");
        CustomFieldValues cfValues = compagneCoupon.getCfValuesNullSafe();

        String couponTypeValue = (String) this.fetchMandatoryCF(cfValues, CouponTemplateDto.CF_COUPON_TYPE);
        log.debug(" generating coupons for type : {}", couponTypeValue);

        switch (CouponTypeEnum.instances.get(couponTypeValue)) {
        case UNIQUE:
               this.generateUnicCoupon(compagneCoupon);
            break;
        case GENERIC:
            this.generateGenericCoupons(compagneCoupon);
        }

        // Once the generation passed successfully , the change the Compagne soupon status to actifve
        log.debug("Passing Coupon {}, status to active  ", compagneCoupon.getCode());

        cfValues.setValue(CouponTemplateDto.CF_STATUS, CouponStatusEnum.ACTIVE.getValue());
        compagneCoupon.setCfValues(cfValues);

    }

	/**
	 * Says whether a coupon is valid or not.
	 * 
	 * @param couponCode the coupon code
	 * @param subscriptionDate the subscription date
	 * @param offerCode the offer code
	 * @return true if the coupon is valid, false otherwise.
	 * @throws MeveoApiException
	 */
	public boolean isCouponValid(String couponCode, String subscriptionDate, String offerCode) throws MeveoApiException {
		
		// Get custom entity
		CustomEntityInstance couponInstanceEntity = customEntityInstanceService.findByCodeByCet(ConstantesVelib.COUPON_INSTANCE, couponCode);
		OfferTemplate actualOffer = null;
		Date date = new Date();
		
		if (couponInstanceEntity == null) {
			throw new MeveoApiException(MeveoApiErrorCodeEnum.ENTITY_DOES_NOT_EXISTS_EXCEPTION, "");
		}
		
		// Get subscription date
		if (subscriptionDate != null) {
			try {
				date = new Date(Long.parseLong(subscriptionDate));
			} catch (NumberFormatException e) {
				throw new VelibApiException(VelibApiErrorCodeEnum.ERR50, e.getMessage());
			}
		}
		
		// Get offer template
		if (offerCode != null) {
			
			List<OfferTemplate> offerTemplates;
			
			try {
				offerTemplates = offerVelibTemplateService.list(offerCode);
				
				if (offerTemplates == null || offerTemplates.isEmpty()) {
					throw new VelibApiException(VelibApiErrorCodeEnum.ERR157, VelibApiErrorCodeEnum.ERR157.getDescription());
				}
				
				if (offerTemplates.size() > 1) {
					throw new VelibApiException(VelibApiErrorCodeEnum.ERR158, VelibApiErrorCodeEnum.ERR158.getDescription());
				}
				
				actualOffer = offerTemplates.get(0);
				
			} catch (ParseException e) {
				throw new VelibApiException(VelibApiErrorCodeEnum.ERR50, e.getMessage());
			}
		}
		
		// Call service
		try {
		    if(!couponVelibService.isCouponValid(couponInstanceEntity, date, actualOffer)) {
                if(couponVelibService.isRemainingQuatityEmpty(couponInstanceEntity)) {
                    throw new BusinessException(VelibApiErrorCodeEnum.ERR159.getDescription());
                } else {
                    throw new BusinessException(VelibApiErrorCodeEnum.ERR156.getDescription());
                }
            }
		    return true;
		} catch (BusinessException e) {
			throw new VelibApiException(VelibApiErrorCodeEnum.ERR50, e.getMessage());
		}
	}

    /**
     * Get duration from coupon template
     *
     * @param codeCoupon
     * @return
     * @throws BusinessException
     */
	public Long getDuration(String codeCoupon) throws BusinessException {

        Long duration = 0l;
        if (!StringUtils.isEmpty(codeCoupon)) {
            CustomEntityInstance couponInstanceEntity = customEntityServiceVelib.findByCodeByCet(ConstantesVelib.CF_COUPON_INSTANCE, codeCoupon);

            EntityReferenceWrapper couponTemplateReference =
                    (EntityReferenceWrapper) customFieldInstanceService.getCFValue(couponInstanceEntity, ConstantesVelib.CF_COUPON_TEMPLATE);

            CustomEntityInstance couponTemplateEntity = customEntityServiceVelib.findByCodeByCet(ConstantesVelib.CF_COUPON_TEMPLATE, couponTemplateReference.getCode());

            CustomFieldValues couponTemplateCF = customEntityServiceVelib.getCfValues(couponTemplateEntity);
            duration = (Long) couponTemplateCF.getValue(ConstantesVelib.CF_COUPON_DURATION);
        } else {
            log.info("use getDuration with empty parameter");
        }
        return duration;
    }
    
    public void activeCoupon(String subscriptionCode) throws BusinessException {

        Subscription subscription = subscriptionService.findByCode(subscriptionCode);

        EntityReferenceWrapper couponInstanceReference =
                (EntityReferenceWrapper) customFieldInstanceService.getCFValue(subscription, ConstantesVelib.CF_SUB_COUPON);

        if (couponInstanceReference != null) {
            CustomEntityInstance couponInstanceEntity = customEntityServiceVelib.findByCodeByCet (ConstantesVelib.CF_COUPON_INSTANCE,  couponInstanceReference.getCode());

            EntityReferenceWrapper couponTemplateReference =
                    (EntityReferenceWrapper) customFieldInstanceService.getCFValue(couponInstanceEntity, ConstantesVelib.CF_COUPON_TEMPLATE);
            if(couponTemplateReference != null ) {
                CustomEntityInstance couponTemplateEntity = customEntityInstanceService.findByCodeByCet(ConstantesVelib.CF_COUPON_TEMPLATE, couponTemplateReference.getCode());
                if(couponTemplateEntity != null) {
                    CustomFieldValues couponInstanceCF = customEntityServiceVelib.getCfValues(couponInstanceEntity);
                    CustomFieldValues couponTemplateCF = customEntityServiceVelib.getCfValues(couponTemplateEntity);

                    // Parameters
                    Long remainingQuantity = (Long) couponInstanceCF.getValue(ConstantesVelib.CF_COUPON_REMAINING_QUANTITY);
                    CouponTypeEnum couponType = CouponTypeEnum.instances.get((String) couponTemplateCF.getValue(ConstantesVelib.CF_COUPON_TYPE));
                    long newRemainingQuantity = 0l;
                    //Désactivation du décrément pour le coupon générique pour raison de performances
                    if (CouponTypeEnum.UNIQUE.equals(couponType)) {
                        if (remainingQuantity > 0) {
                            newRemainingQuantity = remainingQuantity - 1;
                        } else {
                            throw new BusinessException(ConstantesVelib.CF_COUPON_REMAINING_QUANTITY + " already = " + remainingQuantity);
                        }
                    } else {
                        return;
                    }
                    couponInstanceCF.setValue(ConstantesVelib.CF_COUPON_REMAINING_QUANTITY, newRemainingQuantity);
                    couponInstanceEntity.setCfValues(couponInstanceCF);
                }
            }

        }
    }

    private void generateGenericCoupons(CustomEntityInstance compagneCoupon) throws MeveoApiException, BusinessException {

        CustomFieldValues cfValues = compagneCoupon.getCfValuesNullSafe();

        CouponInstanceDto couponInstanceDto = new CouponInstanceDto();
        String prefix = (String) this.fetchMandatoryCF(cfValues, CouponTemplateDto.CF_PREFIX);
        Long quantity = (Long) this.fetchMandatoryCF(cfValues, CouponTemplateDto.CF_QTE);

        couponInstanceDto.setCode(prefix);
        couponInstanceDto.setDescription(prefix);
        couponInstanceDto.setCouponTemplate(compagneCoupon.getCode());
        couponInstanceDto.setInitialQuantity(quantity);
        couponInstanceDto.setRemainingQuantity(quantity);

        customEntityInstanceApi.create(couponInstanceDto.toCustomEntityInstanceDto());

    }

    private void generateUnicCoupon(CustomEntityInstance compagneCoupon) throws MeveoApiException, BusinessException {
        CustomFieldValues cfValues = compagneCoupon.getCfValuesNullSafe();

        Long quantity = (Long) this.fetchMandatoryCF(cfValues, CouponTemplateDto.CF_QTE);
        if (quantity <= 0 ) {
            throw new BusinessApiException("Invalid quantity = " + quantity);
        }

        String prefix = (String) this.fetchMandatoryCF(cfValues, CouponTemplateDto.CF_PREFIX);

        customEntityServiceVelib.generateUnicCoupon(quantity, prefix, compagneCoupon.getCode());
    }

    private CustomEntityInstance validateCompagneCoupon(CouponInstanceRequestDto postData) throws MissingParameterException, EntityDoesNotExistsException, BusinessApiException {

        final String compagneCouponCode = postData.getCode();
        if (StringUtils.isBlank(compagneCouponCode)) {
            missingParameters.add("couponTemplateCode");
        }
        this.handleMissingParameters();

        CustomEntityInstance compagneCoupon = customEntityInstanceService.findByCode(compagneCouponCode);
        if (compagneCoupon == null) {
            throw new EntityDoesNotExistsException(CustomEntityTemplate.class, compagneCouponCode);
        }
        log.debug(" CouponTemplate {} found : {} ", compagneCouponCode, compagneCoupon);

        CustomFieldValues cfValues = compagneCoupon.getCfValuesNullSafe();

        log.debug("Checking Coupon status ...");
        this.validateStatus(cfValues);

        log.debug("Checking coupon validity dates ...");
        Date today = DateUtils.setDateToStartOfDay(new Date());

//        this.validateValidFrom(cfValues, today);
        this.validateValidTo(cfValues, today);

        log.debug(" CompagneCoupon = {} validated", compagneCoupon);
        return compagneCoupon;
    }

    private Object fetchMandatoryCF(CustomFieldValues cfValues, final String cfCode) throws BusinessApiException {
        Object cfValue =  cfValues.getValue(cfCode);
        if (cfValue == null) {
            throw new BusinessApiException(String.format("Value not found for field %s ", cfCode));
        }
        return cfValue;
    }

    private void validateValidTo(CustomFieldValues cfValues, Date today) throws BusinessApiException {
        Object cfValidto =  cfValues.getValue(CouponTemplateDto.CF_VALID_TO);

        if (cfValidto != null) {
            Date validto = (Date) cfValidto;
            if (validto == null || validto.before(today)) {
                throw new BusinessApiException(String.format("Invalid date : [validto = %s  ]", validto));
            }
        } else {
            throw new BusinessApiException("Date validTo value not found");
        }
        log.debug(" validto = {} validated", cfValidto);
    }

    private void validateValidFrom(CustomFieldValues cfValues,  Date today) throws BusinessApiException {

        Object cfValidFrom =  cfValues.getValue(CouponTemplateDto.CF_VALID_FROM);
        if (cfValidFrom != null) {
            Date validFrom = (Date) cfValidFrom;
            if (validFrom == null || validFrom.after(today)) {
                throw new BusinessApiException(String.format("Invalid date : [validFrom = %s  ]", validFrom));
            }
        } else {
            throw new BusinessApiException("Date validFrom value not found");
        }
        log.debug(" validFrom = {} validated", cfValidFrom);
    }

    private void validateStatus(CustomFieldValues cfValues) throws BusinessApiException {
        Object cfStatus =  cfValues.getValue(CouponTemplateDto.CF_STATUS);
        if (cfStatus != null) {
            CouponStatusEnum couponStatus = CouponStatusEnum.instances.get((String) cfStatus);
            if (couponStatus != CouponStatusEnum.ACTIVE) {
                throw new BusinessApiException("Coupon status Should be active");
            }
        } else {
            throw new BusinessApiException("Coupon Status value not found");
        }
        log.debug(" cfStatus = {} validated", cfStatus);
    }

    /**
     * List CouponInstances matching a given criteria
     * @param pagingAndFiltering
     * @return List of CouponInstances
     */
    public CouponInstancesResponseDto list(VelibPagingAndFiltering pagingAndFiltering) throws MeveoApiException {

        CouponInstancesResponseDto result = new CouponInstancesResponseDto();

        PaginationConfiguration paginationConfig = toPaginationConfiguration("id", SortOrder.ASCENDING, null, pagingAndFiltering, CustomEntityInstance.class);
        paginationConfig.getFilters().put("cetCode", CouponInstanceDto.CET_CODE);
        result.setPaging(pagingAndFiltering);

        Map<String, Object> cfFilters  = MapUtils.emptyIfNull(pagingAndFiltering.getCfFilters());

        if (MapUtils.isNotEmpty(cfFilters)) {
           List<String> codes =  this.findCodesByCfValues(cfFilters);
           if (CollectionUtils.isNotEmpty(codes)) {
               paginationConfig.getFilters().put("code", codes);
           } else {
               log.debug("No couponInsatnce for cfFilters = {} ", cfFilters);
               result.getPaging().setTotalNumberOfRecords(0);
               return result;
           }
        }

        Long count = customEntityInstanceService.count(paginationConfig);
        result.getPaging().setTotalNumberOfRecords(count.intValue());

        if (count > 0) {
            List<CustomEntityInstance> couponInstances = customEntityInstanceService.list(paginationConfig);
            CollectionUtils.emptyIfNull(couponInstances).stream().forEach(couponInstance -> {
                result.addItem(new CouponInstanceDto(couponInstance));
            });
        }

        return result;
    }

    /**
     * @param cfFilters
     * @return List of coupon instances's codes , filtered by the CFs values in 'cfFilters' param
     * @throws MeveoApiException
     */
    private List<String> findCodesByCfValues(Map<String, Object> cfFilters) throws MeveoApiException {

        log.debug(" findCodesByCfValues couponTemplateCode = {}", cfFilters);
        List<String> codes = null;

        StringBuilder queryBuilder = new StringBuilder("select cei.code from cust_cei as cei  where cei.cet_code='CouponInstance' ");

        // filtering by couponTemplate code
        String couponTemplateCode = (String) cfFilters.get(CouponInstanceDto.CF_COUPON_TEMPLATE);
        if (StringUtils.isNotBlank(couponTemplateCode)) {
            queryBuilder.append(" and cei.cf_values\\:\\:json -> 'couponTemplate'->0->'entity'->>'classname'  = '").append(CustomEntityInstance.class.getName()).append("'")
                    .append(" and cei.cf_values\\:\\:json -> 'couponTemplate'->0->'entity'->>'code' = '").append(couponTemplateCode).append("'");
        }

        // filtering by remaining quantity
        if (cfFilters.containsKey(CouponInstanceDto.CF_REMAINING_QUANTITY)) {

            Map<String, String> remainingQte = (Map<String, String>) cfFilters.get(CouponInstanceDto.CF_REMAINING_QUANTITY);
            String operation = CF_CRETERIA_OPERATION_MAP.get(remainingQte.get("op"));

            if (operation == null) {
                throw new InvalidParameterException("remainingQuantity.op", remainingQte.toString());
            }

            Long quantity = null;
            try {
                quantity = Long.parseLong(remainingQte.get("value"));
            } catch (NumberFormatException e) {
                throw new InvalidParameterException("remainingQuantity.value", remainingQte.toString());
            }

            queryBuilder.append(" and CAST( cei.cf_values\\:\\:json -> 'remainingQuantity'->0->>'long' as INTEGER) ").append(operation).append(quantity);
        }

        List<Map<String, Object>> result = customEntityInstanceService.executeNativeSelectQuery(queryBuilder.toString(), new HashMap<>());

        if (CollectionUtils.isNotEmpty(result)) {
            codes = result.stream().map(item -> String.valueOf(item.get("code"))).collect(Collectors.toList());
            log.debug(" findCodesByCfValues : {} couponInsatnces found ",codes.size());
        }

        return codes;
    }

}
