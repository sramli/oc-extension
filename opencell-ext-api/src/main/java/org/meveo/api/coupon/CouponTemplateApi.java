package org.meveo.api.coupon;

import static org.meveo.common.VelibPagingAndFiltering.CF_CRETERIA_OPERATION_MAP;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.BaseApi;
import org.meveo.api.CustomEntityInstanceApi;
import org.meveo.api.dto.CustomEntityInstanceDto;
import org.meveo.api.dto.CustomFieldDto;
import org.meveo.api.dto.CustomFieldsDto;
import org.meveo.api.exception.BusinessApiException;
import org.meveo.api.exception.EntityDoesNotExistsException;
import org.meveo.api.exception.InvalidParameterException;
import org.meveo.api.exception.MeveoApiException;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.dto.coupon.CustomEntityInstancesDto;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.crm.custom.CustomFieldInheritanceEnum;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.meveo.security.CurrentUser;
import org.meveo.security.MeveoUser;
import org.meveo.service.catalog.impl.OfferTemplateService;
import org.meveo.service.crm.impl.CustomFieldInstanceService;
import org.meveo.service.custom.CustomEntityInstanceService;
import org.meveo.service.custom.CustomEntityTemplateService;
import org.meveo.service.custom.EntityCustomActionService;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.meveo.common.ConstantesVelib;
import org.meveo.common.CouponDiscountTypeEnum;
import org.meveo.common.CouponStatusEnum;
import org.meveo.common.CouponTypeEnum;
import org.meveo.common.VelibPagingAndFiltering;
import org.meveo.dto.coupon.CouponTemplateDto;

@Stateless
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class CouponTemplateApi extends BaseApi {

    @Inject
    private OfferTemplateService offerTemplateService;

    @Inject
    private CustomEntityInstanceService customEntityInstanceService;

    @Inject
    private CustomEntityInstanceApi customEntityInstanceApi;

    @Inject
    private CustomEntityTemplateService customEntityTemplateService;

    @Inject
    private EntityCustomActionService entityActionScriptService;

    @Inject
    private CustomFieldInstanceService customFieldInstanceService;

    @Inject
    @CurrentUser
    protected MeveoUser currentUser;

    private static final Logger LOGGER = LoggerFactory.getLogger(CouponTemplateApi.class);

    public void createOrUpdate(CouponTemplateDto postData) throws MeveoApiException, BusinessException {
        if (customEntityInstanceService.findByCode(postData.getCode()) == null) {
            this.create(postData);
        } else {
            this.update(postData);
        }
    }

    public void create(CouponTemplateDto postData) throws MeveoApiException, BusinessException {

        this.validateCouponTemplate(postData,false);
        LOGGER.info("Coupon {} created by {}", postData, currentUser.getFullNameOrUserName());
        log.debug(" Creating CouponTemplate ... ");
        customEntityInstanceApi.create(postData.toCustomEntityInstanceDto());
    }

    private void validateCouponTemplate(CouponTemplateDto postData, boolean forUpdate) throws MeveoApiException {

        log.debug(" Checking missingParameters ... ");
        final String code = postData.getCode();
        if (StringUtils.isBlank(code)) {
            missingParameters.add("code");
        }

        final String prefix = postData.getPrefix();
        if (StringUtils.isBlank(prefix)) {
            missingParameters.add("prefix");
        }

        final Long quantity = postData.getQuantity();
        if (quantity != null && quantity.longValue() <= 0) {
            missingParameters.add("quantity");
        }

        this.handleMissingParameters();

        log.debug(" Checking validFrom and validTo values ... ");
        final Date validFrom = postData.getValidFrom();
        final Date validTo = postData.getValidTo();

        if (validFrom != null && validTo != null && validFrom.after(validTo)) {
            throw new BusinessApiException(" Invalid dates : validFrom and  validTo ");
        }

        log.debug(" Setting default enums values ... ");
        final CouponTypeEnum couponType = postData.getCouponType();
        if (couponType == null && !forUpdate) {
            postData.setCouponType(couponType.GENERIC);
        }

        final CouponStatusEnum status = postData.getStatus();
        if (status == null && !forUpdate) {
            postData.setStatus(CouponStatusEnum.DRAFT);
        }

        final CouponDiscountTypeEnum discountType = postData.getDiscountType();
        if (discountType == null && !forUpdate) {
            postData.setDiscountType(CouponDiscountTypeEnum.PERCENT);
        }

        List<String> offers = postData.getOffers();
        log.debug(" Checking offers ...");

        for (String offreCode : offers) {
            OfferTemplate offerTemplate = offerTemplateService.findByCode(offreCode);
            if (offerTemplate == null) {
                throw new EntityDoesNotExistsException(OfferTemplate.class, offreCode);
            }
        }
    }

    public void update(CouponTemplateDto postData) throws MeveoApiException, BusinessException {
        this.validateCouponTemplate(postData, true);
        LOGGER.info("Coupon {} updated by {}", postData, currentUser.getFullNameOrUserName());
        customEntityInstanceApi.update(postData.toCustomEntityInstanceDto());
    }

    /**
     * List coupon template matching filtering and query criteria
     *
     * @param pagingAndFiltering Paging and filtering criteria. Specify "transactions" in fields to include transactions and "pdf" to generate/include PDF document
     * @return A list of invoices
     * @throws InvalidParameterException invalid parameter exception
     */

    public CustomEntityInstancesDto list(VelibPagingAndFiltering pagingAndFiltering) throws MeveoApiException {

        CustomEntityInstancesDto result = new CustomEntityInstancesDto();

        if (pagingAndFiltering == null) {
            pagingAndFiltering = new VelibPagingAndFiltering();
        }
        
        result.setPaging(pagingAndFiltering);

        PaginationConfiguration paginationConfig = toPaginationConfiguration("id", SortOrder.ASCENDING, null, pagingAndFiltering, CustomEntityInstance.class);
        paginationConfig.getFilters().put("cetCode", CouponTemplateDto.CET_CODE);
        result.setPaging(pagingAndFiltering);

        Map<String, Object> cfFilters  = MapUtils.emptyIfNull(pagingAndFiltering.getCfFilters());

        // filtering by CF values :
        if (MapUtils.isNotEmpty(cfFilters)) {
            List<String> codes =  this.findCodesByCfValues(cfFilters);
            if (CollectionUtils.isNotEmpty(codes)) {
                paginationConfig.getFilters().put("code", codes);
            } else {
                log.debug("No couponTemplates for cfFilters = {} ", cfFilters);
                result.getPaging().setTotalNumberOfRecords(0);
                return result;
            }
        }

        Long totalCount = customEntityInstanceService.count(paginationConfig);
        result.getPaging().setTotalNumberOfRecords(totalCount.intValue());

        if (totalCount > 0) {
            List<CustomEntityInstance> customEntityTemplates = customEntityInstanceService.list(paginationConfig);
            for (CustomEntityInstance cei : customEntityTemplates) {
                String count = null;
                if(ConstantesVelib.COUPON_TYPE_GENERIC.equals(cei.getCfValues().getValue(CouponTemplateDto.CF_COUPON_TYPE))) {
                    count = getRemainingQantityGeneric(cei);
                } else {
                    count = getRemainingQantityUnique(cei);
                }

                result.getCustomEntityInstanceDtos().add(customEntityInstanceToDto(cei, count));
            }
        }

        return result;
    }

    private CustomEntityInstanceDto customEntityInstanceToDto(CustomEntityInstance cei, String count) {

        CustomEntityInstanceDto customEntityInstanceDto = null;
        try {
            CustomFieldsDto customFieldInstances = entityToDtoConverter.getCustomFieldsDTO(cei, CustomFieldInheritanceEnum.INHERIT_NONE);
            customEntityInstanceDto = new CustomEntityInstanceDto(cei , customFieldInstances);

            if(count != null) {
                CustomFieldDto customFieldDto = new CustomFieldDto();
                customFieldDto.setStringValue(count);
                customFieldDto.setCode(ConstantesVelib.CF_REMAINING_QTE);
                customEntityInstanceDto.getCustomFields().getCustomField().add(customFieldDto);
            }
        } catch (Exception e) {
            log.error("ERROR", e);
        }
        return customEntityInstanceDto;
    }

    private String getRemainingQantityUnique(CustomEntityInstance cei) throws MeveoApiException {

        StringBuilder queryBuilder = new StringBuilder("select count(*) " +
                "from cust_cei " +
                "where lower(cet_code)='couponinstance' " +
                "and cf_values\\:\\:json -> 'remainingQuantity' -> 0 ->> 'long' = '1' " +
                "and cf_values\\:\\:json -> 'couponTemplate' -> 0 -> 'entity' ->> 'code' = '" + cei.getCode() + "' " +
                "group by description");


        List<Map<String, Object>> result = customEntityInstanceService.executeNativeSelectQuery(queryBuilder.toString(), new HashMap<>());

        List<String> count = null ;
        if (CollectionUtils.isNotEmpty(result)) {
            count = result.stream().map(item -> String.valueOf(item.get("count"))).collect(Collectors.toList());
        }

        return count != null && count.get(0) != null ? count.get(0) : "";

    }

    private String getRemainingQantityGeneric(CustomEntityInstance cei) throws MeveoApiException {

        StringBuilder queryBuilder = new StringBuilder("select json_array_elements (CAST(cf_values as JSON)->'remainingQuantity') ->> 'long' remainingquantity" +
                " from cust_cei as cei " +
                "where lower(cei.cet_code)='couponinstance' " +
                "and cei.cf_values\\:\\:json -> 'couponTemplate' -> 0 -> 'entity' ->> 'code' = '"+ cei.getCode() +"'");

        List<Map<String, Object>> result = customEntityInstanceService.executeNativeSelectQuery(queryBuilder.toString(), new HashMap<>());

        List<String> remainingQuantity = null ;
        if (CollectionUtils.isNotEmpty(result)) {
            remainingQuantity = result.stream().map(item -> String.valueOf(item.get("remainingquantity"))).collect(Collectors.toList());
        }

        return remainingQuantity != null && remainingQuantity.get(0) != null ? remainingQuantity.get(0) : "";
    }

    /**
     * @param cfFilters
     * @return List of coupon templates's codes , filtered by the CFs values in 'cfFilters' param
     * @throws MeveoApiException
     */
    private List<String> findCodesByCfValues(Map<String, Object> cfFilters) throws MeveoApiException {

        log.debug(" findCodesByCfValues cfFilters = {}", cfFilters);
        List<String> codes = null;

        StringBuilder queryBuilder = new StringBuilder("select cei.code from cust_cei as cei  where cei.cet_code='CouponTemplate' ");

        // filtering by status :
        final String status = (String) cfFilters.get(CouponTemplateDto.CF_STATUS);
        if (StringUtils.isNotBlank(status)) {
            queryBuilder.append(" and cei.cf_values\\:\\:json -> 'status'->0->>'string' = '").append(status).append("'");
        }

        // filtering by coupon type :
        final String couponType = (String) cfFilters.get(CouponTemplateDto.CF_COUPON_TYPE);
        if (StringUtils.isNotBlank(couponType)) {
            queryBuilder.append(" and cei.cf_values\\:\\:json -> 'couponType'->0->>'string' = '").append(couponType).append("'");
        }

        // filtering by date valid from :
        if (cfFilters.containsKey(CouponTemplateDto.CF_VALID_FROM)) {
            Map<String, String> validFromParam = (Map<String, String>) cfFilters.get(CouponTemplateDto.CF_VALID_FROM);
            String operation = CF_CRETERIA_OPERATION_MAP.get(validFromParam.get("op"));

            queryBuilder.append(" and cast ( cei.cf_values\\:\\:json -> 'validFrom'->0->>'date' as date) ")
                    .append(operation).append(" to_date('").append(validFromParam.get("value")).append("' ,'YYYY-MM-DD')");
        }

        // filtering by date valid to :
        if (cfFilters.containsKey(CouponTemplateDto.CF_VALID_TO)) {
            Map<String, String> validToParam = (Map<String, String>) cfFilters.get(CouponTemplateDto.CF_VALID_TO);
            String operation = CF_CRETERIA_OPERATION_MAP.get(validToParam.get("op"));

            queryBuilder.append(" and cast ( cei.cf_values\\:\\:json -> 'validTo'->0->>'date' as date) ")
                    .append(operation).append(" to_date('").append(validToParam.get("value")).append("' ,'YYYY-MM-DD')");
        }

        List<Map<String, Object>> result = customEntityInstanceService.executeNativeSelectQuery(queryBuilder.toString(), new HashMap<>());

        if (CollectionUtils.isNotEmpty(result)) {
            codes = result.stream().map(item -> String.valueOf(item.get("code"))).collect(Collectors.toList());
            log.debug(" findCodesByCfValues : {} couponTemplates found ",codes.size());
        }

        return codes;
    }

}
