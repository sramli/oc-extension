package org.meveo.report;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.meveo.common.ConstantesVelib;
import org.meveo.commons.utils.CsvBuilder;
import org.meveo.commons.utils.EjbUtils;
import org.meveo.commons.utils.ParamBean;
import org.meveo.model.crm.Provider;
import org.meveo.service.crm.impl.ProviderService;

public class CSVFilesReportJobUtils {

	private static final char DEFAULT_SEPARATOR = ';';

	private static String csvFile;
	private static CsvBuilder csvBuilder;
	private static Map<String, String[]> nameColumnsByJobNameMap = new HashMap<>();
	private static ProviderService providerService = (ProviderService) EjbUtils
			.getServiceInterface(ProviderService.class.getSimpleName());
	private static Provider appProvider;

	static {
		nameColumnsByJobNameMap.put(ConstantesVelib.REPORT_JOB_RETAKE_BONUS,
				new String[] { "customerAccountCode", "bonus_2017", "bonus_2018", ConstantesVelib.CF_KM_TOTAL_CODE,
						ConstantesVelib.CF_KM_ELECTRIQUE_CODE, ConstantesVelib.CF_TRIP_COUNTER_IN_MONTH_CODE,
						ConstantesVelib.CF_TRIPS_AVERAGE_DURATION_IN_MONTH_CODE,
						ConstantesVelib.CF_TRIPS_HIGHEST_DISTANCE_IN_MONTH_CODE });
		
		nameColumnsByJobNameMap.put(ConstantesVelib.REPORT_JOB_REFUND,
				new String[] { "","Compte utilisateur",					
						"Refund period start",
						"Refund period end",
						"Wallet id",
						"Offer Code",
						"Wallet amount with tax",
						"Wallet amount without tax",
						"Wallet start",
						"Wallet end",
						"Wallet Nb days",										
						"Wallet refund start",
						"Wallet refund end",
						"Wallet refund Nb days",						
						"Wallet refund amount with tax",
						"Wallet refund amount without tax",						
						"Result",
						"Comments"});
		
		nameColumnsByJobNameMap.put(ConstantesVelib.REPORT_JOB_REFUND_VELIB,
				new String[] { "","Compte utilisateur",
						"Wallet id",								
						"Wallet refund start",
						"Wallet refund end",
						"Wallet refund Op date",
						"Wallet refund amount with tax",
						"Wallet refund amount without tax",						
						"Result",
						"Comments"});
		
		appProvider = providerService.getProvider();
	}

	private CSVFilesReportJobUtils() {

	}

	public static String initializeCSVFile(String jobName) throws IOException {
		// get the Date of the job execution
		Date date = new Date();
		SimpleDateFormat simpleFormatDate = new SimpleDateFormat("dd-MM-yyyy");
		String dateCreateFile = simpleFormatDate.format(date);
		
		SimpleDateFormat simpleFormatDateTime = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		String dateTimeCreateFile = simpleFormatDateTime.format(date);		

		// get the absolutePath for the file
		csvFile = initializeCSVPath(dateCreateFile, dateTimeCreateFile, jobName);

		// initialize the csvBuilder
		initializeCVSBuilder(jobName);
		
		return csvFile;
	}

	private static void initializeCVSBuilder(String jobName) {

		// initialize the cvsBuilder
		csvBuilder = new CsvBuilder(";", false);

		// Set the header of the csv file
		setHeader(jobName);
	}

	private static String initializeCSVPath(String createDate, String createDateTime, String jobName) {
		StringBuilder filePathName = new StringBuilder(
				ParamBean.getInstance().getProperty("providers.rootDir", "/CR_Job/") + File.separator
						+ appProvider.getCode());
		filePathName.append(ConstantesVelib.REPORT_JOB_ABSOLUTE_PATH).append(File.separator).append(createDate)
				.append(File.separator).append(jobName).append("_").append(createDateTime).append(".csv").toString();
		return filePathName.toString();
	}

	public static void writeLine(String[] values) throws IOException {
		writeLine(values, DEFAULT_SEPARATOR, ' ');
	}

	private static String followCVSformat(String value) {
		String result = value;
		if (result.contains("\"")) {
			result = result.replace("\"", "\"\"");
		}
		return result;
	}

	private static void writeLine(String[] values, char separators, char customQuote) throws IOException {

		boolean first = true;

		if (separators == ' ') {
			separators = DEFAULT_SEPARATOR;
		}

		StringBuilder sb = new StringBuilder();
		for (String value : values) {
			if (!first) {
				sb.append(separators);
			}
			if (customQuote == ' ') {
				sb.append(followCVSformat(value));
			} else {
				sb.append(customQuote).append(followCVSformat(value)).append(customQuote);
			}

			first = false;
		}
		sb.append("\n");
		csvBuilder.appendValue(sb.toString());
	}

	public static void csvBuilderToFile() throws IOException {
		if (csvBuilder != null) {
			csvBuilder.toFile(csvFile);
		}
	}

	/**
	 * set the header of the csv file
	 * 
	 * @param csvBuilder
	 * @return csvBuilder
	 */
	private static void setHeader(String jobName) {
		csvBuilder.appendValues(nameColumnsByJobNameMap.get(jobName)).appendValue("\n");
	}

}
