package org.meveo.async;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import org.meveo.job.RefundVelibProcessBean;
import org.meveo.model.jobs.JobExecutionResultImpl;

@Stateless
public class RefundVelibAsync {

	@Inject
	private RefundVelibProcessBean refundVelibProcessBean;

	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public Future<String> launchAndForget(List<String> lines, JobExecutionResultImpl result, Date startDate,
			Date endDate, Date refundOperationDate, Boolean fakeRun, List<String[]> listReportFile, String chargeCode) {
		for (String line : lines) {
			listReportFile.add(refundVelibProcessBean.execute(result, line, startDate, endDate, refundOperationDate, fakeRun, chargeCode));
		}
		return new AsyncResult<>("OK");
	}

}
