package org.meveo.exception;

import org.meveo.api.ApiErrorCodeEnum;
import org.meveo.api.exception.MeveoApiException;

import org.meveo.common.VelibApiErrorCodeEnum;

public class VelibApiException  extends MeveoApiException {

    private ApiErrorCodeEnum errorCode;

    public VelibApiException() {
        this.errorCode = VelibApiErrorCodeEnum.GENERIC_API_EXCEPTION;
    }

    public VelibApiException(Throwable e) {
        super(e);
        this.errorCode = VelibApiErrorCodeEnum.GENERIC_API_EXCEPTION;
    }

    public VelibApiException(VelibApiErrorCodeEnum errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public VelibApiException(String message) {
        super(message);
    }

}
