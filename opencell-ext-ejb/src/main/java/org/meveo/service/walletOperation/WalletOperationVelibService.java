package org.meveo.service.walletOperation;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;

import org.apache.commons.lang3.StringUtils;
import org.meveo.commons.utils.QueryBuilder;
import org.meveo.dto.walletoperation.JustificatifPaiementDto;
import org.meveo.model.billing.WalletOperation;
import org.meveo.service.base.PersistenceService;

import com.fasterxml.jackson.databind.ObjectMapper;

@Stateless
public class WalletOperationVelibService extends PersistenceService<WalletOperation> {

	public static final String queryWalletOperationsByPaymentReference() {
		StringBuilder stringBuilder = new StringBuilder("SELECT subcat.code AS code, "
				+ "ot.description AS description, wo.operation_date AS operationDate, "
				+ "wo.start_date AS startDate, wo.end_date AS endDate, wo.amount_with_tax AS montant, "
				+ "ct.description AS chargeLabel, ct.description_i18n AS chargeLabelI18n, inv.invoice_date AS invoiceDate, wo.parameter_3 AS parameter3, "
				+ "ci.subscription_id AS subscriptionId ");

		stringBuilder.append("FROM billing_wallet_operation AS wo ");

		stringBuilder.append("JOIN cat_offer_template AS ot ON ot.code = wo.offer_code ");
		stringBuilder.append("JOIN billing_charge_instance AS ci ON ci.id = wo.charge_instance_id ");
		stringBuilder.append("JOIN cat_charge_template AS ct ON ct.id = ci.charge_template_id ");
		stringBuilder.append("JOIN billing_invoice_sub_cat AS subcat ON subcat.id = ct.invoice_sub_category ");
		stringBuilder.append("JOIN billing_rated_transaction AS brt ON brt.wallet_operation_id = wo.id ");
		stringBuilder.append("JOIN billing_invoice AS inv ON inv.id = brt.invoice_id AND inv.invoice_number IN ");
		stringBuilder.append("( ");
		stringBuilder.append("SELECT distinct ao.reference AS reference FROM ar_account_operation AS ao ");
		stringBuilder.append("JOIN ar_matching_amount AS ama ON ama.account_operation_id = ao.id ");
		stringBuilder.append("JOIN ar_matching_amount AS ama2 ON ama2.matching_code_id = ama.matching_code_id ");
		stringBuilder.append("JOIN ar_account_operation AS ao2 ON ama2.account_operation_id = ao2.id AND ao2.reference =:paymentReference ");
		stringBuilder.append("WHERE ao.transaction_type = 'I' ");
		stringBuilder.append("); ");

		return stringBuilder.toString();
	}

	/**
	 * this method joins the EDR and WalletOperation table to list the
	 * walletOperation by given period, subscriptionCode, internalCode on the
	 * EDR table
	 * 
	 * @param begin
	 * @param end
	 * @param customerInternalcode
	 * @param subscriptionCode
	 * @return List<WalletOperation>
	 */
	@SuppressWarnings("unchecked")
	public List<WalletOperation> findWalletOperations(Date begin, Date end, String customerInternalcode, String subscriptionCode) {

		StringBuilder sb = new StringBuilder("select walletOperation from org.meveo.model.billing.WalletOperation walletOperation, org.meveo.model.rating.EDR edr")
				.append(" where walletOperation.edr.id=edr.id and edr.accessCode =:customerInternalcode ")
				.append(" and edr.dateParam3>=:begin and  edr.dateParam3 <=:end and edr.subscription.code =:subscriptionCode ");

		return new QueryBuilder(sb.toString()).getQuery(getEntityManager())
				.setParameter("begin", begin)
				.setParameter("end", end)
				.setParameter("customerInternalcode", customerInternalcode)
				.setParameter("subscriptionCode", subscriptionCode)
				.getResultList();

	}

	public List<WalletOperation> findWalletOperations(String subscriptionCode, String chargeCode) {

		StringBuilder sb = new StringBuilder("SELECT wo FROM WalletOperation wo join wo.chargeInstance ci")
				.append(" WHERE ci.subscription.code=:subscriptionCode AND ci.code =:chargeCode");

		return new QueryBuilder(sb.toString()).getQuery(getEntityManager())
				.setParameter("subscriptionCode", subscriptionCode)
				.setParameter("chargeCode", chargeCode)
				.getResultList();

	}


	/**
	 * Search WalletOperation (charge) from the payment
	 * 
	 * @param paymentReference
	 *            the reference of Payment entity
	 * @param chosenLanguageCode
	 * 		      code of the language chosen by the customer
	 * @return the wallet operation corresponding to the payment reference
	 */
	@SuppressWarnings("unchecked")
	public List<JustificatifPaiementDto> findWalletOperationsByPaymentReference(String paymentReference, String chosenLanguageCode) {
		String query = queryWalletOperationsByPaymentReference();
		List<Object[]> resultLines = getEntityManager().createNativeQuery(query).setParameter("paymentReference", paymentReference).getResultList();
		
		return resultLines.stream().map(line -> resultQueryToJustificatifPaiementDTO(line, chosenLanguageCode)).collect(Collectors.toList());
	}

	private JustificatifPaiementDto resultQueryToJustificatifPaiementDTO(Object[] resultLine, String chosenLanguageCode) {
		SimpleDateFormat sDateFormatFromDataBase = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		JustificatifPaiementDto justificatifPaiementDto = new JustificatifPaiementDto();

		justificatifPaiementDto.setCode(resultLine[0] != null ? resultLine[0].toString() : "");
		justificatifPaiementDto.setDescription(resultLine[1] != null ? resultLine[1].toString() : "");
		if (resultLine[5] != null) {
			justificatifPaiementDto.setMontant(new BigDecimal(resultLine[5].toString()));
		}
		if (resultLine[6] != null) {
			justificatifPaiementDto.setChargeLabel(resultLine[6].toString());
		}
		try {
			if (resultLine[2] != null)
				justificatifPaiementDto.setOperationDate(sDateFormatFromDataBase.parse(resultLine[2].toString()));
			if (resultLine[3] != null)
				justificatifPaiementDto.setStartDate(sDateFormatFromDataBase.parse(resultLine[3].toString()));
			if (resultLine[4] != null)
				justificatifPaiementDto.setEndDate(sDateFormatFromDataBase.parse(resultLine[4].toString()));
			// Try to get charge label by chosen language
			if (resultLine[7] != null) {
				String chargeLabelI18n = resultLine[7].toString();
				ObjectMapper mapper = new ObjectMapper();
				Map<String, String> i18nMap = mapper.readValue(chargeLabelI18n, Map.class);
				String translatedChargeLabel = i18nMap.get(chosenLanguageCode);
				if (!StringUtils.isEmpty(translatedChargeLabel)) {
					justificatifPaiementDto.setChargeLabel(translatedChargeLabel);
				}
			}
			if (resultLine[8] != null)
				justificatifPaiementDto.setInvoiceDate(sDateFormatFromDataBase.parse(resultLine[8].toString()));
			if (resultLine[9] != null)
				justificatifPaiementDto.setParameter3(resultLine[9].toString());
			// Subscription id
			if (resultLine[10] != null) {
				justificatifPaiementDto.setSubscriptionId(resultLine[10].toString());
			}
		} catch (ParseException e) {
			String message = "Erreur de parsing de la date du justificatif de paiement {}";
			log.error(message, justificatifPaiementDto.getCode());
			throw new RuntimeException(message, e);
		} catch (IOException e) {
			String message = "Impossible d'analyser la description i18n de la charge.";
			log.error(message, justificatifPaiementDto.getCode());
			throw new RuntimeException(message, e);
		}

		return justificatifPaiementDto;
	}

	public List<Long> getwalletOperationByChargeInstanceVelib(Long chargeId) {
		QueryBuilder qb = new QueryBuilder(WalletOperation.class, "m", null);
		qb.addCriterion("chargeInstance.id", "=", chargeId, true);
		try {
			return qb.getIdQuery(getEntityManager()).getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
}
