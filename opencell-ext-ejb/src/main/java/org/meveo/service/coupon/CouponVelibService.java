package org.meveo.service.coupon;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.common.ConstantesVelib;
import org.meveo.common.CouponStatusEnum;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.crm.EntityReferenceWrapper;
import org.meveo.model.crm.custom.CustomFieldValues;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.meveo.service.crm.impl.CustomFieldInstanceService;
import org.meveo.service.custom.CustomEntityInstanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
public class CouponVelibService {

    @Inject
    private CustomEntityInstanceService customEntityInstanceService;

    @Inject
    private CustomFieldInstanceService customFieldInstanceService;

    private static final Logger log = LoggerFactory.getLogger(CouponVelibService.class);

    public boolean isCouponValid(CustomEntityInstance couponInstanceEntity, Date subscriptionDate, OfferTemplate actualOffer)  throws BusinessException {
        return isCouponValid(couponInstanceEntity, subscriptionDate, actualOffer, false);
    }

    public boolean isCouponValid(CustomEntityInstance couponInstanceEntity, Date subscriptionDate, OfferTemplate actualOffer, boolean isReimbursment)  throws BusinessException {

        if(couponInstanceEntity == null) {
            log.info("coupon instance is null");
            return false;
        }

        // Extract customTemplateEntity
        Object couponTemplateRefCf = couponInstanceEntity.getCfValues().getValue(ConstantesVelib.CF_COUPON_TEMPLATE);
        if(couponTemplateRefCf == null) {
            log.info("entity reference is null");
            return false;
        }

        EntityReferenceWrapper couponTemplateReference = (EntityReferenceWrapper) couponTemplateRefCf;
        CustomEntityInstance couponTemplateEntity = customEntityInstanceService.findByCodeByCet(ConstantesVelib.CF_COUPON_TEMPLATE, couponTemplateReference.getCode());
        if(couponTemplateEntity == null) {
            throw new BusinessException("couponInstance or couponTemplate is null");
        }

        CustomFieldValues couponInstanceCF = couponInstanceEntity.getCfValues();
        CustomFieldValues couponTemplateCF = couponTemplateEntity.getCfValues();
        if(couponInstanceCF == null || couponTemplateCF == null) {
            throw new BusinessException("couponInstanceCF or couponTemplateCF is null");
        }

        // Parameters
        Long remainingQuantity = (Long) couponInstanceCF.getValue(ConstantesVelib.CF_COUPON_REMAINING_QUANTITY);
        Date validFrom = (Date) couponTemplateCF.getValue(ConstantesVelib.CF_COUPON_VALID_FROM);
        Date validTo = (Date) couponTemplateCF.getValue(ConstantesVelib.CF_COUPON_VALID_TO);
        String status = (String) couponTemplateCF.getValue(ConstantesVelib.CF_COUPON_STATUS);
        List<EntityReferenceWrapper> offers = (List<EntityReferenceWrapper>) couponTemplateCF.getValue(ConstantesVelib.CF_COUPON_OFFERS);

        // loggers
        log.info("remainingQuantity {}", remainingQuantity);
        log.info("validFrom {}", validFrom);
        log.info("validTo {}", validTo);
        log.info("status {}", status);
        log.info("subscriptionDate {}", subscriptionDate);

        // todo Renvoyer une erreur si un paramètre obligatoire est null
        if(remainingQuantity == null || status == null || offers == null || offers.isEmpty()) {
            log.info("mandatory field null or empty");
            return false;
        }


        // Condition on the remainig quantity
        if(remainingQuantity.compareTo(0L) <= 0 && !isReimbursment) {
            log.info("remainigQuantity <= 0");
            return false;
        }

        // Conditions on validFrom and validTo
        if(subscriptionDate == null || subscriptionDate.before(validFrom) || subscriptionDate.after(validTo)) {
            // ERR Le code promo renseigné est invalide ou expiré.
            log.info("subscriptionDate not in the coupon period");
            return false;
        }

        // Check couponTemplate status
        if(couponTemplateEntity.isDisabled() || !status.equals(CouponStatusEnum.ACTIVE.getValue())) {
            log.info("couponTemplate status not active");
            return false;
        }

        // Check couponInstance status
        if(couponInstanceEntity.isDisabled()) {
            log.info("couponInstance not active");
            return false;
        }

        // Check if the current offer is compatible with the coupon
        if(actualOffer != null && !containsOffer(offers, actualOffer)) {
            log.debug("actualOffer is not in the coupon offer list");
            return false;
        }

        return true;
    }

    private boolean containsOffer(List<EntityReferenceWrapper> offers, OfferTemplate offer) {
        for(int i = 0; i < offers.size(); i++) {
            if (offers.get(i).getCode().equals(offer.getCode())) {
                return true;
            }
        }
        return false;
    }

    public boolean isRemainingQuatityEmpty(CustomEntityInstance couponInstanceEntity) throws BusinessException {
        CustomFieldValues couponInstanceCF = couponInstanceEntity.getCfValues();
        if(couponInstanceCF == null) {
            throw new BusinessException("couponInstanceCF or couponTemplateCF is null");
        }
        Long remainingQuantity = (Long) couponInstanceCF.getValue(ConstantesVelib.CF_COUPON_REMAINING_QUANTITY);
        if(remainingQuantity == null) {
            log.info("mandatory field null or empty");
            return false;
        }

        return !(remainingQuantity > 0); // return true if empty
    }

}
