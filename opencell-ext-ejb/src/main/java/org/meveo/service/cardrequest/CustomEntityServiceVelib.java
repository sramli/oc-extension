package org.meveo.service.cardrequest;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.api.CustomEntityInstanceApi;
import org.meveo.api.exception.MeveoApiException;
import org.meveo.common.VelibStringUtils;
import org.meveo.commons.utils.QueryBuilder;
import org.meveo.dto.coupon.CouponInstanceDto;
import org.meveo.model.crm.custom.CustomFieldValues;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.meveo.service.base.BusinessService;
import org.meveo.service.custom.CustomEntityInstanceService;

@Stateless
public class CustomEntityServiceVelib extends BusinessService<CustomEntityInstance> {

	@Inject
	private CustomEntityInstanceService customEntityInstanceService;

	@Inject
	private CustomEntityInstanceApi customEntityInstanceApi;

	/**
	 * the method searches for a customer's card requests
	 * 
	 * @param cetCode
	 * @param codeCustomer
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<CustomEntityInstance> findByCodeEntityAndcodeCustomer(String cetCode, String codeCustomer) {

		QueryBuilder qb = new QueryBuilder(getEntityClass(), "cei", null);
		qb.addCriterion("cei.cetCode", "=", cetCode, true);
		qb.addCriterion("cei.description", " like ", "%" + codeCustomer + "%", true);

		return qb.getQuery(getEntityManager()).getResultList();
	}

	/**
	 * find CustomEntityInstance By Code and By customEntity
	 *
	 * @param customEntity
	 * @param code
	 * @return
	 * @throws BusinessException
	 */
	public CustomEntityInstance findByCodeByCet (String customEntity,  String code) throws BusinessException {
		CustomEntityInstance customEntityInstance = customEntityInstanceService.findByCodeByCet(customEntity, code);
		if (customEntityInstance == null) {
			throw new BusinessException("CustomEntity " + customEntity + " with code "+ code + " not found");
		}
		return customEntityInstance;
	}

	/**
	 *  get CfValues of customEntityInstance
	 *
	 * @param customEntityInstance
	 * @return
	 * @throws BusinessException
	 */
	public CustomFieldValues getCfValues (CustomEntityInstance customEntityInstance) throws BusinessException {
		if (customEntityInstance != null) {
			CustomFieldValues customFieldValues = customEntityInstance.getCfValues();
			if (customFieldValues == null) {
				throw new BusinessException("CfValues of " + customEntityInstance.getCode() +  " is null");
			}
			return customFieldValues;
		} else {
			throw new BusinessException("The customEntityInstance parameter is null");
		}
	}

	public void generateUnicCoupon(final Long quantity, final String prefix, final String couponTemplateCode) throws MeveoApiException, BusinessException {

		for (int i = 0; i < quantity.intValue(); i++) {

			if (i > 0 && i % 10 == 0) {
				getEntityManager().flush();
				getEntityManager().clear();
			}

			CouponInstanceDto couponInstanceDto = new CouponInstanceDto();
			couponInstanceDto.setCode(prefix + VelibStringUtils.generateUniqueKey());
			couponInstanceDto.setDescription(prefix);
			couponInstanceDto.setCouponTemplate(couponTemplateCode);
			couponInstanceDto.setInitialQuantity(Long.valueOf(1));
			couponInstanceDto.setRemainingQuantity(Long.valueOf(1));

			customEntityInstanceApi.create(couponInstanceDto.toCustomEntityInstanceDto());
			log.debug(" {} CouponInstance created ", i);
		}
	}

}
