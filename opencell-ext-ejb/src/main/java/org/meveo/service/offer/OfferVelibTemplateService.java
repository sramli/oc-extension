package org.meveo.service.offer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.service.catalog.impl.GenericProductOfferingService;

/**
 * Offer Template service implementation.
 *
 */
@Stateless
public class OfferVelibTemplateService extends GenericProductOfferingService<OfferTemplate> {

	/**
	 * The method create the filters based on the code (if there's one), the
	 * status (active and enabled) and the validity. The offers concerned are the
	 * ones that are enabled at the moment.
	 *
	 * @param code offer code (can be null)
	 * @return the list of the offers meeting the criters
	 * @throws ParseException exception during the date parsing
	 */
	public List<OfferTemplate> list(String code) throws ParseException {
		Map<String, Object> filters = new HashMap<>();

		if (!StringUtils.isBlank(code)) {
			filters.put("code", code);
		}

		// Get today at 00:00:00 to include all the offer with today being the last day
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date today = simpleDateFormat.parse(simpleDateFormat.format(new Date()));

		filters.put("minmaxRange validity.from validity.to", today);

		PaginationConfiguration config = new PaginationConfiguration(filters);
		return list(config);

	}

}