package org.meveo.job;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.meveo.admin.async.SubListCreator;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.job.logging.JobLoggingInterceptor;
import org.meveo.async.RefundVelibAsync;
import org.meveo.common.ConstantesVelib;
import org.meveo.commons.utils.ParamBean;
import org.meveo.interceptor.PerformanceInterceptor;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.model.jobs.JobInstance;
import org.meveo.model.shared.DateUtils;
import org.meveo.report.CSVFilesReportJobUtils;
import org.meveo.service.catalog.impl.OneShotChargeTemplateService;
import org.meveo.service.crm.impl.CustomFieldInstanceService;
import org.slf4j.Logger;

@Stateless
public class RefundVelibJobBean {

	@Inject
	private CustomFieldInstanceService customFieldInstanceService;

	@Inject
	private Logger log;
	
	@Inject
	private RefundVelibAsync refundVelibAsync;
	
	@Inject
	private OneShotChargeTemplateService oneShotChargeTemplateService;

	private static final String IMPORT_DIR = ParamBean.getInstance()
			.getProperty("subscription.refund.script.import.directory", null);

	@SuppressWarnings("unchecked")
	@Interceptors({ JobLoggingInterceptor.class, PerformanceInterceptor.class })
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public void execute(JobExecutionResultImpl result, JobInstance jobInstance) {

		List<String[]> listReportFile = new ArrayList<>();

		try {
			Long nbRuns;
			Long waitingMillis;
			Boolean fakeRun = false;
			String fileName = "";
			String startDateString = "";
			String endDateString = "";
			String operationDateString = "";
			String chargeCode = "";
			Date startDate = null;
			Date endDate = null;
			Date operationDate = null;
			try {
				nbRuns = (Long) customFieldInstanceService.getCFValue(jobInstance, "nbRuns");
				waitingMillis = (Long) customFieldInstanceService.getCFValue(jobInstance, "waitingMillis");
				fakeRun = ((String) customFieldInstanceService.getCFValue(jobInstance, "fake_run"))
						.equalsIgnoreCase("true");
				if (nbRuns == -1) {
					nbRuns = (long) Runtime.getRuntime().availableProcessors();
				}

			} catch (Exception e) {
				nbRuns = 1L;
				waitingMillis = 0L;
				fakeRun = true;
				log.warn("Cant get customFields for {} {}", jobInstance.getJobTemplate(), e.getMessage());
			}

			try {
				fileName = (String) customFieldInstanceService.getCFValue(jobInstance, "refundFileName");
				result.addReport("REFUND_IMPORT_FILE_NAME : "+fileName + " ");
			} catch (Exception e) {
				log.error("error parsing refundFileName. Please specify a correct string");
				result.registerError("error parsing refundFileName. Please specify a correct string");
				return;
			}

			try {
				startDateString = (String) customFieldInstanceService.getCFValue(jobInstance, "startDate");
				startDate = DateUtils.parseDateWithPattern(startDateString, DateUtils.DATE_TIME_PATTERN);
				result.addReport("REFUND_START_DATE : "+startDate + " ");
			} catch (Exception e) {
				log.error("error parsing StartDate. Please specify a correct format Date");
				result.registerError("error parsing StartDate. Please specify a correct format Date");
				return;
			}

			try {
				endDateString = (String) customFieldInstanceService.getCFValue(jobInstance, "endDate");
				endDate = DateUtils.parseDateWithPattern(endDateString, DateUtils.DATE_TIME_PATTERN);
				result.addReport("REFUND_END_DATE : "+endDate+ " ");
			} catch (Exception e) {
				log.error("error parsing EndDate. Please specify a correct format Date");
				result.registerError("error parsing EndDate. Please specify a correct format Date");
				return;
			}

			try {
				operationDateString = (String) customFieldInstanceService.getCFValue(jobInstance, "operationDate");
				operationDate = DateUtils.parseDateWithPattern(operationDateString, DateUtils.DATE_TIME_PATTERN);
				result.addReport("REFUND_OPERATION_DATE : "+operationDate+ " ");
			} catch (Exception e) {
				log.error("error parsing OperationDate. Please specify a correct format Date");
				result.registerError("error parsing OperationDate. Please specify a correct format Date");
				return;
			}
			
			try {
				chargeCode = (String) customFieldInstanceService.getCFValue(jobInstance, "chargeCode");
				result.addReport("REFUND_CHARGE_CODE : "+chargeCode + " ");
				
				// Check Charge Code Validity
				if (oneShotChargeTemplateService.findByCode(chargeCode) == null) {
					log.error("error Invalid ChargeCode. Please specify an existing chargeCode");
					result.registerError("error Invalid ChargeCode. Please specify an existing chargeCode");
					return;
				}			
				
			} catch (Exception e) {
				log.error("error parsing ChargeCode. Please specify a correct string");
				result.registerError("error parsing ChargeCode. Please specify a correct charge Code");
				return;
			}

	
			StringBuilder filePath = new StringBuilder();
			filePath.append(IMPORT_DIR);
			filePath.append(File.separator);
			filePath.append(fileName);
			
			result.addReport("REFUND_IMPORTE_FAKE_RUN : " + fakeRun.toString() + " ");
			result.addReport("Fichier d'import : "+filePath);

			if (!controlImportFile(filePath)) {
				result.registerError("[E] Impossible d'ouvrir le fichier d'import: " + filePath);
				return;
			}

			List<String> lines = readImportFile(filePath);
			
			log.info("Nombre de ligne lu dans le fichier d'entrée : {}",lines.size());

			String csvFilePath = CSVFilesReportJobUtils.initializeCSVFile(ConstantesVelib.REPORT_JOB_REFUND_VELIB);
			CSVFilesReportJobUtils.csvBuilderToFile();
			log.info("[I] Fichier de Rapport : {}", csvFilePath);
			result.addReport("Fichier de Rapport :" + csvFilePath);

			List<Future<String>> futures = new ArrayList<Future<String>>();
			SubListCreator subListCreator = new SubListCreator(lines, nbRuns.intValue());
			log.debug("block to run : {}", subListCreator.getBlocToRun());
			
			log.debug("nbThreads : {}", nbRuns);
			while (subListCreator.isHasNext()) {
				futures.add(refundVelibAsync.launchAndForget((List<String>) subListCreator.getNextWorkSet(), result, startDate, 
						endDate, operationDate, fakeRun, listReportFile, chargeCode));
				if (subListCreator.isHasNext()) {
					try {
						Thread.sleep(waitingMillis.longValue());
					} catch (InterruptedException e) {
						log.error("", e);
					}
				}
			}
			// Wait for all async methods to finish
			for (Future<String> future : futures) {
				try {
					future.get();
				} catch (InterruptedException e) {
					// It was cancelled from outside - no interest

				} catch (ExecutionException e) {
					Throwable cause = e.getCause();
					result.registerError(cause.getMessage());
					log.error("[E] Failed to execute async method", cause);
				}
			}

		} catch (Exception e) {
			
		}
		
		log.info("[I] +++ listReportFile.size() = {}", listReportFile.size());
		
		try {
			for(String[] lineReport : listReportFile)
				CSVFilesReportJobUtils.writeLine(lineReport);
			CSVFilesReportJobUtils.csvBuilderToFile();
		} catch (IOException e) {
			log.warn("[W] Erreur lors de la génération d'une ligne de rapport d'execution. {}", e);
		}
	}

	/**
	 * Check that the provided import file name, really exists and can be opened.
	 * 
	 * @param filePath Import filepath
	 * @throws BusinessException Import filepath doesn't exist.
	 */
	public boolean controlImportFile(StringBuilder filePath) throws BusinessException {
		// Check if the file exists
		if (!Paths.get(filePath.toString()).toFile().exists()) {
			final String message = "[E] Impossible d'ouvrir le fichier d'import: {}";
			log.error(message, filePath);
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	public List<String> readImportFile(StringBuilder filePath) throws IOException {

		List<String> lines = new LinkedList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(filePath.toString()))) {
			String line = br.readLine();
			while (line != null && !line.isEmpty()) {
				if (line.chars().allMatch(Character::isDigit)) {
					lines.add(line);
				}

				// read next line
				line = br.readLine();
			}
		}
		return lines;
	}
}
