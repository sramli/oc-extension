package org.meveo.job;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.service.walletOperation.WalletOperationVelibService;
import org.meveo.model.billing.OneShotChargeInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.billing.WalletOperation;
import org.meveo.model.billing.WalletOperationStatusEnum;
import org.meveo.model.catalog.OneShotChargeTemplate;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.service.billing.impl.OneShotChargeInstanceService;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.billing.impl.UserAccountService;
import org.meveo.service.billing.impl.WalletOperationService;
import org.meveo.service.catalog.impl.OneShotChargeTemplateService;
import org.slf4j.Logger;

@Stateless
public class RefundVelibProcessBean {

    @Inject
    UserAccountService userAccountService;

    @Inject
    WalletOperationService walletOperationService;

    @Inject
    private WalletOperationVelibService walletOperationVelibService;

    @Inject
    SubscriptionService subscriptionService;

    @Inject
    OneShotChargeInstanceService oneShotChargeInstanceService;

    @Inject
    OneShotChargeTemplateService oneShotChargeTemplateService;

    @Inject
    private Logger log;

    private static final String REPORT_TYPE_ERROR = "ERROR";

    private static final String REPORT_TYPE_WARNING = "WARNING";

    private static final String REPORT_TYPE_SUCCESS = "SUCCESS";

    /**
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public String[] execute(JobExecutionResultImpl result, String userId, Date startDate, Date endDate,
                             Date refundOperationDate, Boolean fakeRun, String chargeCode) {

        // Get the user ID userAccount entity with all its wallets operations related to
        // annual/monthly recurrent charges or subscription refunds.
        UserAccount userAccount = userAccountService.findByCode(userId, Arrays.asList("subscriptions"));

        if (userAccount == null) {
            return prvReport(result, REPORT_TYPE_WARNING, userId, "", startDate, endDate, refundOperationDate, "0.00",
                    "0.00", "Le cpt utilisateur a provoque une exception et ne sera pas traite.");
        }

        if (userAccount.getSubscriptions() == null || userAccount.getSubscriptions().isEmpty()) {
            return prvReport(result, REPORT_TYPE_ERROR, userAccount.getCode(), "", startDate, endDate,
                    refundOperationDate, "0.00", "0.00", "Pas de souscription trouvee pour cet utilisateur");
        }

        if (fakeRun) {
            return prvReport(result, REPORT_TYPE_SUCCESS, userAccount.getCode(), "", startDate, endDate,
                    refundOperationDate,"0.00", "0.00",
                    "Mode simulation -> Creation de la wallet operation de remboursement ignoree.");
        } else {
            return processUserAccountToCreateWalletRefund(result, userAccount, startDate, endDate, refundOperationDate, chargeCode);
        }
    }

    private String[] prvReport(JobExecutionResultImpl result, String reportType, String userId, String walletId,
                               Date refundWalletStartDate, Date refundWalletEndDate, Date refundWalletOperationDate,
                               String amountToRefundWithTax, String amountToRefundWithoutTax, String msg) {

        String resultReport = "";

        if (REPORT_TYPE_ERROR.equals(reportType)) {
            result.addNbItemsProcessedWithError(1);
            String logMsg = prvBuildReportComment(userId, walletId, msg);
            resultReport = REPORT_TYPE_ERROR;
            log.error("{}", logMsg);
        } else if (REPORT_TYPE_WARNING.equals(reportType)) {
            result.addNbItemsProcessedWithWarning(1);
            String logMsg = prvBuildReportComment(userId, walletId, msg);
            resultReport = REPORT_TYPE_WARNING;
            log.warn("{}", logMsg);
        } else if (REPORT_TYPE_SUCCESS.equals(reportType)) {
            result.addNbItemsCorrectlyProcessed(1);
            String logMsg = prvBuildReportComment(userId, walletId, msg);
            resultReport = REPORT_TYPE_SUCCESS;
            log.info("{}", logMsg);
        }

        return new String[] { "[" + userId + "]", // user account
                walletId, // Wallet id
                refundWalletStartDate == null ? "" : refundWalletStartDate.toString(), // Wallet refund start
                refundWalletEndDate == null ? "" : refundWalletEndDate.toString(), // Wallet refund end
                refundWalletOperationDate == null ? "" : refundWalletOperationDate.toString(), // Wallet refund end
                amountToRefundWithTax, // Wallet refund amount with tax
                amountToRefundWithoutTax, // Wallet refund amount without tax
                resultReport, // result
                msg }; // comments
    }

    private String prvBuildReportComment(String userId, String walletId, String msg) {
        String cmt = "cpt utilisateur: " + userId;
        if ((walletId != null) && !walletId.isEmpty()) {
            cmt += ", Wallet Op: " + walletId;
        }
        cmt += " - " + msg;
        return cmt;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private String[] processUserAccountToCreateWalletRefund(JobExecutionResultImpl result, UserAccount userAccount,
                                                            Date refundWalletStartDate, Date refundWalletEndDate, Date refundWalletOperationDate, String chargeCode) {


        // Create the charge template to be used to generate the refund wallet operation
        OneShotChargeTemplate oneShotChargeTemplate = oneShotChargeTemplateService.findByCode(chargeCode);
        if (oneShotChargeTemplate == null) {
            return prvReport(result, REPORT_TYPE_ERROR, userAccount.getCode(), "", refundWalletStartDate,
                    refundWalletEndDate, refundWalletOperationDate, "0.00", "0.00",
                    "Erreur lors de la récupération du template de la charge");
        }

        // Get the subscription associated to the user account
        Subscription subscription = subscriptionService.findByCode(userAccount.getSubscriptions().get(0).getCode());
        if (subscription == null) {
            return prvReport(result, REPORT_TYPE_ERROR, userAccount.getCode(), "", refundWalletStartDate,
                    refundWalletEndDate, refundWalletOperationDate, "0.00", "0.00",
                    "Erreur lors de la récupération de la souscription");
        }


        // Create the one shot charge for the refund and apply charge (wallet operation creation)
        OneShotChargeInstance oneShotChargeInstance = null;
        try {
            oneShotChargeInstance = oneShotChargeInstanceService.oneShotChargeApplication(subscription, (OneShotChargeTemplate) oneShotChargeTemplate,
                    null, // walletCode
                    refundWalletOperationDate, // OperationDate
                    BigDecimal.ZERO, // AmountWithoutTax
                    BigDecimal.ZERO, // AmountWithTax
                    BigDecimal.ONE, // Quantity
                    null, // Criteria1
                    null, // Criteria2
                    null, // Criteria3
                    null, // Description
                    subscription.getOrderNumber(), // OrderNumber
                    true); // applyCharge
        } catch (BusinessException e) {
            return prvReport(result, REPORT_TYPE_ERROR, userAccount.getCode(), "", refundWalletStartDate,
                    refundWalletEndDate, refundWalletOperationDate, "0.00", "0.00",
                    "Erreur lors de la creation de la WalletOperation de remboursement");
        }

        if (oneShotChargeInstance == null) {
            return prvReport(result, REPORT_TYPE_ERROR, userAccount.getCode(), "", refundWalletStartDate,
                    refundWalletEndDate, refundWalletOperationDate, "0.00", "0.00",
                    "Erreur lors de la récupération de la oneShotChargeInstance");
        }

        // Get the generated wallet ID to update its Start/End dates and operation Date
        List<Long> ids = walletOperationVelibService.getwalletOperationByChargeInstanceVelib(oneShotChargeInstance.getId());
        WalletOperation walletOperationReimbursement = null;

        if (!ids.isEmpty() && ids.size() == 1) {
            walletOperationReimbursement = walletOperationService.findById(ids.get(0));
        }

        if (walletOperationReimbursement == null) {
            return prvReport(result, REPORT_TYPE_ERROR, userAccount.getCode(), "", refundWalletStartDate,
                    refundWalletEndDate, refundWalletOperationDate, "0.00", "0.00",
                    "Erreur lors de la recuperation de la WalletOperation de remboursement");
        }

        walletOperationReimbursement.setStartDate(refundWalletStartDate);
        walletOperationReimbursement.setEndDate(refundWalletEndDate);
        walletOperationReimbursement.setOperationDate(refundWalletOperationDate);
        walletOperationReimbursement.setStatus(WalletOperationStatusEnum.RESERVED);

        try {
            walletOperationService.update(walletOperationReimbursement);
        } catch (BusinessException e) {
            return prvReport(result, REPORT_TYPE_ERROR, userAccount.getCode(), "", refundWalletStartDate,
                    refundWalletEndDate, refundWalletOperationDate, "0.00", "0.00",
                    "Erreur lors de la mise à jour de la WalletOperation de remboursement");
        }

        return prvReport(result, REPORT_TYPE_SUCCESS, userAccount.getCode(),
                walletOperationReimbursement.getId().toString(), refundWalletStartDate, refundWalletEndDate,
                refundWalletOperationDate, "0.00", "0.00", "Wallet operation de remboursement generee avec succes.");
    }

}
